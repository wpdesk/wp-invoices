<?php declare(strict_types = 1);

$ignoreErrors = [];
$ignoreErrors[] = [
	// identifier: method.notFound
	'message' => '#^Call to an undefined method WPDesk\\\\Invoices\\\\Documents\\\\Pdf\\:\\:createDocumentForOrder\\(\\)\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxCreateHandler.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Ajax\\\\AjaxCreateHandler\\:\\:handleAjaxRequest\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxCreateHandler.php',
];
$ignoreErrors[] = [
	// identifier: assign.propertyType
	'message' => '#^Property WPDesk\\\\Invoices\\\\Ajax\\\\AjaxCreateHandler\\:\\:\\$documentCreator \\(WPDesk\\\\Invoices\\\\Documents\\\\Pdf\\) does not accept WPDesk\\\\Invoices\\\\Documents\\\\Creator\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxCreateHandler.php',
];
$ignoreErrors[] = [
	// identifier: method.notFound
	'message' => '#^Call to an undefined method WC_Email\\:\\:trigger\\(\\)\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxEmailHandler.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxEmailHandler.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Ajax\\\\AjaxEmailHandler\\:\\:handleAjaxRequest\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxEmailHandler.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Ajax\\\\AjaxGetPdfHandler\\:\\:handleAjaxRequest\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxGetPdfHandler.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Ajax\\\\AjaxHandler\\:\\:handleAjaxRequest\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Ajax/AjaxHandler.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setAddress\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setAddress2\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setCity\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setCompanyName\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setCountry\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setEmail\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setFirstName\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setLastName\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setPhone\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setPostCode\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\ClientData\\:\\:setVatNumber\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/ClientData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:addItem\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setClientData\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setCurrency\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setIssueDate\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setItems\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setPaidAmount\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setPaymentMethod\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setPricesIncludeTax\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setSaleDate\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\InvoiceData\\:\\:setZeroTax\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/InvoiceData.php',
];
$ignoreErrors[] = [
	// identifier: method.notFound
	'message' => '#^Call to an undefined method WC_Order_Item\\:\\:get_total\\(\\)\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: method.notFound
	'message' => '#^Call to an undefined method WC_Order_Item\\:\\:get_total_tax\\(\\)\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:addAdditionalData\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setName\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setOrderItem\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setPrice\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setPriceFromOrderItem\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setQuantity\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setTaxClass\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setTaxClassFromTaxes\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setTaxClassFromTaxes\\(\\) has parameter \\$taxes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setTaxRate\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setTaxRateFromTaxes\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setTaxRateFromTaxes\\(\\) has parameter \\$taxes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:setUnitOfMeasure\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: argument.type
	'message' => '#^Parameter \\#1 \\$orderItem of class WPDesk\\\\Invoices\\\\Data\\\\Items\\\\FeeItem constructor expects WC_Order_Item_Fee, WC_Order_Item given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: argument.type
	'message' => '#^Parameter \\#1 \\$orderItem of class WPDesk\\\\Invoices\\\\Data\\\\Items\\\\LineItem constructor expects WC_Order_Item_Product, WC_Order_Item given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: argument.type
	'message' => '#^Parameter \\#1 \\$orderItem of class WPDesk\\\\Invoices\\\\Data\\\\Items\\\\Shippingtem constructor expects WC_Order_Item_Shipping, WC_Order_Item given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\Data\\\\Items\\\\InvoiceItem\\:\\:\\$additionalData type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/InvoiceItem.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 3,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/LineItem.php',
];
$ignoreErrors[] = [
	// identifier: cast.useless
	'message' => '#^Casting to string something that\'s already string\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:__construct\\(\\) has parameter \\$taxes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: return.type
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:getRate\\(\\) should return float but returns WPDesk\\\\Invoices\\\\Data\\\\Items\\\\floatg\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:getRatesDataFromTotals\\(\\) has parameter \\$taxes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:getRatesDataFromTotals\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:removeEmptyRates\\(\\) has parameter \\$total with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:removeEmptyRates\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:setRate\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:setRate\\(\\) has parameter \\$taxes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: booleanNot.exprNotBoolean
	'message' => '#^Only booleans are allowed in a negated boolean, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: property.defaultValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:\\$rate \\(WPDesk\\\\Invoices\\\\Data\\\\Items\\\\floatg\\) does not accept default value of type float\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: class.notFound
	'message' => '#^Property WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:\\$rate has unknown class WPDesk\\\\Invoices\\\\Data\\\\Items\\\\floatg as its type\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\Data\\\\Items\\\\TaxRate\\:\\:\\$taxes type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/Items/TaxRate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:addDefault\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:addDefault\\(\\) has parameter \\$value with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:getDefaults\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:setFromData\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:setFromData\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:\\$defaults type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: property.onlyWritten
	'message' => '#^Property WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:\\$documentType is never read, only written\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: property.onlyWritten
	'message' => '#^Property WPDesk\\\\Invoices\\\\Data\\\\OrderDefaults\\:\\:\\$order is never read, only written\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Data/OrderDefaults.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\AbstractCreator\\:\\:createDocumentForOrder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/AbstractCreator.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\AbstractCreator\\:\\:createDocumentForOrder\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/AbstractCreator.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\Creator\\:\\:createDocumentForOrder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/Creator.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\Creator\\:\\:createDocumentForOrder\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/Creator.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\Type\\:\\:setCreator\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/Type.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\Type\\:\\:setDocumentView\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/Type.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\Type\\:\\:setMetaBoxCreateButtonLabel\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/Type.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Documents\\\\Type\\:\\:setParametersLabel\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/Type.php',
];
$ignoreErrors[] = [
	// identifier: notIdentical.alwaysTrue
	'message' => '#^Strict comparison using \\!\\=\\= between \'\' and array will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Documents/Type.php',
];
$ignoreErrors[] = [
	// identifier: function.alreadyNarrowedType
	'message' => '#^Call to function is_object\\(\\) with WC_Order will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: if.alwaysTrue
	'message' => '#^If condition is always true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Email\\\\DocumentEmail\\:\\:setTemplatePath\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Email\\\\DocumentEmail\\:\\:setTemplatePath\\(\\) has parameter \\$templatePath with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Email\\\\DocumentEmail\\:\\:trigger\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: booleanNot.exprNotBoolean
	'message' => '#^Only booleans are allowed in a negated boolean, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: ternary.condNotBoolean
	'message' => '#^Only booleans are allowed in a ternary operator condition, WC_DateTime\\|null given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: if.condNotBoolean
	'message' => '#^Only booleans are allowed in an if condition, WC_Order given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: property.onlyWritten
	'message' => '#^Property WPDesk\\\\Invoices\\\\Email\\\\DocumentEmail\\:\\:\\$templatePath is never read, only written\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: function.unresolvableReturnType
	'message' => '#^Return type of call to function absint contains unresolvable type\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/DocumentEmail.php',
];
$ignoreErrors[] = [
	// identifier: class.notFound
	'message' => '#^Call to method addStringAttachment\\(\\) on an unknown class PHPMailer\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/StringAttachments.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Email\\\\StringAttachments\\:\\:__construct\\(\\) has parameter \\$attachments with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/StringAttachments.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Email\\\\StringAttachments\\:\\:addAction\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/StringAttachments.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Email\\\\StringAttachments\\:\\:addStringAttachments\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/StringAttachments.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Email\\\\StringAttachments\\:\\:removeAction\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/StringAttachments.php',
];
$ignoreErrors[] = [
	// identifier: class.notFound
	'message' => '#^Parameter \\$phpmailer of method WPDesk\\\\Invoices\\\\Email\\\\StringAttachments\\:\\:addStringAttachments\\(\\) has invalid type PHPMailer\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/StringAttachments.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\Email\\\\StringAttachments\\:\\:\\$attachments type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Email/StringAttachments.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Exception\\\\UnknownDocumentTypeException\\:\\:__construct\\(\\) has parameter \\$documentType with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Exception/UnknownDocumentTypeException.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addAddressReplacements\\(\\) has parameter \\$args with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addAddressReplacements\\(\\) has parameter \\$fields with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addAddressReplacements\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addAdminBillingField\\(\\) has parameter \\$fields with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addAdminBillingField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addFieldToFormattedBillingAddress\\(\\) has parameter \\$fields with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addFieldToFormattedBillingAddress\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addFieldToLocalisationAddressFormats\\(\\) has parameter \\$formats with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addFieldToLocalisationAddressFormats\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addMetaDataToOrder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:addMetaDataToOrder\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:appendFieldToWCBillingFields\\(\\) has parameter \\$fields with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:appendFieldToWCBillingFields\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:prepareAdminField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:prepareCheckoutField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:setExcludeFromCheckout\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: property.onlyRead
	'message' => '#^Property WPDesk\\\\Invoices\\\\Field\\\\FormField\\:\\:\\$textDomain is never written, only read\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/FormField.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/InvoiceAsk.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\InvoiceAsk\\:\\:addAddressReplacements\\(\\) has parameter \\$args with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/InvoiceAsk.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\InvoiceAsk\\:\\:addAddressReplacements\\(\\) has parameter \\$fields with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/InvoiceAsk.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\InvoiceAsk\\:\\:addAddressReplacements\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/InvoiceAsk.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\InvoiceAsk\\:\\:isFieldCheckedFromArgs\\(\\) has parameter \\$args with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/InvoiceAsk.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\InvoiceAsk\\:\\:prepareAdminField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/InvoiceAsk.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\InvoiceAsk\\:\\:prepareCheckoutField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/InvoiceAsk.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/VatNumber.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\VatNumber\\:\\:addAddressReplacements\\(\\) has parameter \\$args with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/VatNumber.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\VatNumber\\:\\:addAddressReplacements\\(\\) has parameter \\$fields with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/VatNumber.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\VatNumber\\:\\:addAddressReplacements\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/VatNumber.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\VatNumber\\:\\:prepareAdminField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/VatNumber.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Field\\\\VatNumber\\:\\:prepareCheckoutField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Field/VatNumber.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Integration\\:\\:createDocumentForOrder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Integration.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Integration\\:\\:createDocumentForOrder\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Integration.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Integration\\:\\:getSupportedDocumentType\\(\\) has parameter \\$typeName with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Integration.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Integration\\:\\:setApiClient\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Integration.php',
];
$ignoreErrors[] = [
	// identifier: property.onlyWritten
	'message' => '#^Property WPDesk\\\\Invoices\\\\Metabox\\\\Fields\\\\PaymentDateField\\:\\:\\$payment_date_for_cod is never read, only written\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/Fields/PaymentDateField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxField\\:\\:__construct\\(\\) has parameter \\$id with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxField\\:\\:__construct\\(\\) has parameter \\$label with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxField\\:\\:__construct\\(\\) has parameter \\$name with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxField\\:\\:prepareField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxField\\:\\:render\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxField.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldDate\\:\\:prepareField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldDate.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldPrice\\:\\:prepareField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldPrice.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldSelect\\:\\:__construct\\(\\) has parameter \\$options with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldSelect.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldSelect\\:\\:getOptions\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldSelect.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldSelect\\:\\:prepareField\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldSelect.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldSelect\\:\\:render\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldSelect.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldSelect\\:\\:setOptions\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldSelect.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldSelect\\:\\:setOptions\\(\\) has parameter \\$options with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldSelect.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldSelect\\:\\:\\$options type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldSelect.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\MetaBoxFieldTextarea\\:\\:render\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/MetaBoxFieldTextarea.php',
];
$ignoreErrors[] = [
	// identifier: function.alreadyNarrowedType
	'message' => '#^Call to function is_array\\(\\) with array will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:addMetaBox\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:addMetaBoxCssToHeader\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:addMetaBoxScriptToHeader\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:get_order_for_meta_box\\(\\) has parameter \\$object with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:render\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:render\\(\\) has parameter \\$post_or_order_object with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:renderMetaBoxCreateDocumentContentForOrder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:renderMetaBoxDocumentContentForOrder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:setAjaxCreateHandler\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:setAjaxEmailHandler\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaBox\\:\\:setAjaxGetPdfHandler\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaBox.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaboxFields\\:\\:addMetaBoxField\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaboxFields.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metabox\\\\OrderMetaboxFields\\:\\:setMetaBoxFields\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/OrderMetaboxFields.php',
];
$ignoreErrors[] = [
	// identifier: argument.type
	'message' => '#^Parameter \\#1 \\$text of function esc_attr expects string, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/views/html-metabox-created-document.php',
];
$ignoreErrors[] = [
	// identifier: variable.undefined
	'message' => '#^Variable \\$createdLabelNone might not be defined\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/views/html-metabox-created-none.php',
];
$ignoreErrors[] = [
	// identifier: variable.undefined
	'message' => '#^Variable \\$createdDivId might not be defined\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/views/html-metabox-created.php',
];
$ignoreErrors[] = [
	// identifier: variable.undefined
	'message' => '#^Variable \\$createdLabel might not be defined\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/views/html-metabox-created.php',
];
$ignoreErrors[] = [
	// identifier: variable.undefined
	'message' => '#^Variable \\$parametersDivId might not be defined\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metabox/views/html-metabox-parameters-header.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\CustomMetadata\\:\\:__construct\\(\\) has parameter \\$custom_meta_data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/CustomMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\CustomMetadata\\:\\:__construct\\(\\) has parameter \\$metaDataName with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/CustomMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\CustomMetadata\\:\\:get\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/CustomMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\CustomMetadata\\:\\:update\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/CustomMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\CustomMetadata\\:\\:update\\(\\) has parameter \\$metaData with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/CustomMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\Metadata\\\\CustomMetadata\\:\\:\\$custom_meta_data type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/CustomMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setErrorCode\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setErrorMessage\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setId\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setMetadataContent\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setNumber\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setTypeName\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setTypeNameLabel\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:setWarningMessage\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: property.onlyWritten
	'message' => '#^Property WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:\\$code is never read, only written\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: isset.property
	'message' => '#^Property WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:\\$errorMessage \\(string\\) in isset\\(\\) is not nullable\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: isset.property
	'message' => '#^Property WPDesk\\\\Invoices\\\\Metadata\\\\DocumentMetadata\\:\\:\\$warningMessage \\(string\\) in isset\\(\\) is not nullable\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/DocumentMetadata.php',
];
$ignoreErrors[] = [
	// identifier: function.notFound
	'message' => '#^Function array_is_list not found\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\MetadataContent\\:\\:__construct\\(\\) has parameter \\$metaDataName with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\MetadataContent\\:\\:filter_errors_except_last\\(\\) has parameter \\$metadatas with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\MetadataContent\\:\\:filter_errors_except_last\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\MetadataContent\\:\\:get\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: return.type
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\MetadataContent\\:\\:get\\(\\) should return array but returns mixed\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\MetadataContent\\:\\:update\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\Metadata\\\\MetadataContent\\:\\:update\\(\\) has parameter \\$metaData with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Metadata/MetadataContent.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Order\\\\DocumentView\\:\\:render\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/DocumentView.php',
];
$ignoreErrors[] = [
	// identifier: function.alreadyNarrowedType
	'message' => '#^Call to function is_array\\(\\) with array will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/MyAccount.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/MyAccount.php',
];
$ignoreErrors[] = [
	// identifier: else.unreachable
	'message' => '#^Else branch is unreachable because previous condition is always true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/MyAccount.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Order\\\\MyAccount\\:\\:maybeViewDocumentForType\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/MyAccount.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Order\\\\MyAccount\\:\\:viewDocuments\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/MyAccount.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\Order\\\\OrderDocumentView\\:\\:render\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/OrderDocumentView.php',
];
$ignoreErrors[] = [
	// identifier: if.condNotBoolean
	'message' => '#^Only booleans are allowed in an if condition, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/Order/OrderDocumentView.php',
];
$ignoreErrors[] = [
	// identifier: function.alreadyNarrowedType
	'message' => '#^Call to function is_array\\(\\) with array will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: empty.notAllowed
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\OrdersTable\\\\OrderColumn\\:\\:get_order_for_meta_box\\(\\) has parameter \\$object with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\OrdersTable\\\\OrderColumn\\:\\:maybeRenderInvoiceAskField\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\OrdersTable\\\\OrderColumn\\:\\:orderColumnContent\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\OrdersTable\\\\OrderColumn\\:\\:orderColumnContent\\(\\) has parameter \\$order_id with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\OrdersTable\\\\OrderColumn\\:\\:orderColumns\\(\\) has parameter \\$columns with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Method WPDesk\\\\Invoices\\\\OrdersTable\\\\OrderColumn\\:\\:orderColumns\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\OrdersTable\\\\OrderColumn\\:\\:renderColumnDocumentContentForOrder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: if.condNotBoolean
	'message' => '#^Only booleans are allowed in an if condition, mixed given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/OrderColumn.php',
];
$ignoreErrors[] = [
	// identifier: if.condNotBoolean
	'message' => '#^Only booleans are allowed in an if condition, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/OrdersTable/views/html-column-created-document.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\WooCommerce\\\\DocumentCreatorForOrderStatus\\:\\:lock_process\\(\\) has parameter \\$order_id with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/WooCommerce/DocumentCreatorForOrderStatus.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\WooCommerce\\\\DocumentCreatorForOrderStatus\\:\\:maybe_create_document\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/WooCommerce/DocumentCreatorForOrderStatus.php',
];
$ignoreErrors[] = [
	// identifier: missingType.parameter
	'message' => '#^Method WPDesk\\\\Invoices\\\\WooCommerce\\\\DocumentCreatorForOrderStatus\\:\\:release_lock\\(\\) has parameter \\$order_id with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/WooCommerce/DocumentCreatorForOrderStatus.php',
];
$ignoreErrors[] = [
	// identifier: missingType.iterableValue
	'message' => '#^Property WPDesk\\\\Invoices\\\\WooCommerce\\\\DocumentCreatorForOrderStatus\\:\\:\\$order_queue type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/WooCommerce/DocumentCreatorForOrderStatus.php',
];
$ignoreErrors[] = [
	// identifier: missingType.return
	'message' => '#^Method WPDesk\\\\Invoices\\\\WooCommerce\\\\Integration\\:\\:initApiClient\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/WPDesk/Invoices/WooCommerce/Integration.php',
];

return ['parameters' => ['ignoreErrors' => $ignoreErrors]];
