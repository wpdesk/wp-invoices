<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>
<style>
	body.woocommerce_page_wc-orders .invoice-document,
	body.post-type-shop_order .invoice-document {
		margin-top: 10px;
		padding-top: 10px;
		border-top: 1px solid #dfdfdf;
	}

	body.woocommerce_page_wc-orders h4.invoices-created,
	body.post-type-shop_order h4.invoices-created {
		padding-bottom: 10px;
		border-bottom: 1px solid #dfdfdf;
	}

	body.woocommerce_page_wc-orders .invoice-create-button,
	body.post-type-shop_order .invoice-create-button {
		width: auto;
		text-align: center;
	}

	body.woocommerce_page_wc-orders .invoice-email-button,
	body.post-type-shop_order .invoice-email-button {
		float: right;
		display: inline-block;
		margin-bottom: 3px;
	}

	body.woocommerce_page_wc-orders span.invoice-parameters,
	body.post-type-shop_order span.invoice-parameters {
		margin-top: 5px;
		margin-left: 10px;
		display: inline-block;
	}

	body.woocommerce_page_wc-orders .invoice-parameters select,
	body.post-type-shop_order .invoice-parameters select {
		width: 95%;
	}

	body.woocommerce_page_wc-orders .invoice-email-sent,
	body.post-type-shop_order .invoice-email-sent {
		color: green !important;
		font-weight: bold;
	}

	body.woocommerce_page_wc-orders .invoice-email-error,
	body.post-type-shop_order .invoice-email-error {
		color: red !important;
		font-weight: bold;
	}

	body.woocommerce_page_wc-orders .invoice-link-warning,
	body.post-type-shop_order .invoice-link-warning {
		color: red !important;
		font-weight: bold;
	}
</style>
