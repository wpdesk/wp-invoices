<?php
/**
 * @var string $divId
 * @var string $buttonId
 * @var string $buttonLabelAlt
 * @var string $dataType
 * @var string $nonce
 * @var string $orderId
 * @var string $confirmationMessage
 * @var string $buttonLabel
 * @var string $parametersId
 * @var string $parmaetersLabel
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

?><div class="invoice-document" id="<?php echo esc_attr( $divId ); ?>">
	<a
		id="<?php echo esc_attr( $buttonId ); ?>"
		href="#"
		class="button tips invoice-create-button"
		title="<?php echo esc_html( $buttonLabelAlt ); ?>"
		data-tip="<?php echo esc_attr( $buttonLabelAlt ); ?>"
		data-type="<?php echo esc_attr( $dataType ); ?>"
		data-order_id="<?php echo esc_attr( $orderId ); ?>"
		data-nonce="<?php echo esc_attr( $nonce ); ?>"
		data-confirmation="<?php echo esc_attr( $confirmationMessage ); ?>"
	><?php echo esc_html( $buttonLabel ); ?></a>
	<span class="invoice-parameters"><a
			href="#"
			id="<?php echo esc_attr( $parametersId ); ?>"
			data-type="<?php echo esc_attr( $dataType ); ?>"
		><?php echo esc_html( $parmaetersLabel ); ?> <span class="invoice-expand-parameters">[+]</span></a></span>
</div>
