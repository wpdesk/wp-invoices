<?php
/**
 * @var string $typeNameLabel
 * @var string $number
 * @var string $getPdfUrl
 * @var string $buttonEmailLabelAlt
 * @var string $metadata
 * @var int    $orderId
 * @var string $emailNonce
 * @var string $emailClass
 * @var string $fileName
 * @var string $linkClass
 * @var string $linkTip
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?><div>
	<strong><?php echo esc_html( $typeNameLabel ); ?>:</strong>
	<a
		href="#"
		class="button tips invoice-email-button"
		title="<?php echo esc_attr( $buttonEmailLabelAlt ); ?>"
		data-tip="<?php echo esc_attr( $buttonEmailLabelAlt ); ?>"
		data-order_id="<?php echo esc_attr( $orderId ); ?>"
		data-metadata="<?php echo esc_attr( $metadata ); ?>"
		data-nonce="<?php echo esc_attr( $emailNonce ); ?>"
		data-email_class="<?php echo esc_attr( $emailClass ); ?>"
		data-file_name="<?php echo esc_attr( $fileName ); ?>"
	>@</a>
	<a
		href="<?php echo esc_attr( $getPdfUrl ); ?>"
		class="<?php echo esc_attr( $linkClass ); ?>"
		title="<?php echo esc_attr( $linkTip ); ?>"
		data-tip="<?php echo esc_attr( $linkTip ); ?>"
		target="_blank"
	><?php echo esc_html( $number ); ?></a>
</div>
<div class="clear"></div>
