<?php
/**
 * @var string $typeNameLabel
 * @var string $errorMessage
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?><div>
	<strong><?php echo esc_html( $typeNameLabel ); ?>:</strong>
	<span class="error-message"><?php echo wp_kses_post( $errorMessage ); ?></span>
</div>
