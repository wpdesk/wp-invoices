<?php
//phpcs:ignoreFile
/**
 * @var string $ajaxUrl
 * @var string $ajaxAction
 * @var string $ajaxActionEmail
 * @var string $integrationId
 * @var string $metaboxId
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>
<script type="text/javascript">
	(function ($) {
		function MetaboxScriptClass(ajaxUrl, ajaxInvoiceAction, ajaxEmailAction, integrationId, metaboxId) {
			this.ajaxUrl = ajaxUrl;
			this.ajaxInvoiceAction = ajaxInvoiceAction;
			this.ajaxEmailAction = ajaxEmailAction;
			this.integrationId = integrationId;
			this.metaboxId = metaboxId;
		}

		MetaboxScriptClass.prototype = {
			invoices_ajax_in_progress: false,

			ajaxUrl: '',
			ajaxInvoiceAction: '',
			ajaxEmailAction: '',
			integrationId: '',
			metaboxId: '',

			create_invoice_event: function (event) {
				let button_element = event.target;
				let self = this;

				if (this.invoices_ajax_in_progress) {
					return false;
				}
				let confirmation = $(button_element).data('confirmation');
				if (confirmation) {
					if (!confirm(confirmation)) {
						return;
					}
				}
				this.invoices_ajax_in_progress = true;

				let data = this.prepare_invoice_data(button_element);
				let inside = $(button_element).closest('.inside');

				$(button_element)
					.closest('.inside')
					.find('div.invoices-created')
					.html('<div class="total_row"><p class="wide"><span class="spinner" style="display:inline; margin-bottom:-2px; visibility:visible;"></span></p><div class="clear"></div></div>');

				$.post(self.ajaxUrl, data, function (response) {
					self.invoices_ajax_in_progress = false;
					inside.find('div.invoices-created').html('');
					if (response.status === 'error') {
						alert(response.message);
						inside.html(response.content);
						self.tiptip_and_date_picker();
					} else if (response.status === 'ok') {
						inside.html(response.content);
						self.tiptip_and_date_picker();
					} else {
						alert('Ups! Something go wrong!');
					}
				}).fail(function (xhr, status, error) {
					alert(error);
					inside.find('div.invoices-created').html(error);
					self.invoices_ajax_in_progress = false;
				});
				return false;
			},

			send_email_event: function send_email_event(event) {
				let button_element = event.target;
				let self = this;

				if (this.invoices_ajax_in_progress) {
					return false;
				}
				this.invoices_ajax_in_progress = true;

				let data = this.prepare_email_data(button_element);

				$.post(self.ajaxUrl, data, function (response) {
					self.invoices_ajax_in_progress = false;
					if (response.status === 'error') {
						alert(response.message);
						$(button_element).addClass('invoice-email-error');
					} else if (response.status === 'ok') {
						$(button_element).addClass('invoice-email-sent');
					} else {
						alert('Ups! Something go wrong!');
						$(button_element).addClass('invoice-email-error');
					}
				}).fail(function (xhr, status, error) {
					alert(error);
					$(button_element).addClass('invoice-email-error');
					self.invoices_ajax_in_progress = false;
				});
				return false;
			},

			prepare_invoice_data: function (button_element) {
				let document_type = $(button_element).data('type');
				let nonce = $(button_element).data('nonce');
				let order_id = $(button_element).data('order_id');
				let data = {
					'action': this.ajaxInvoiceAction,
					'type': document_type,
					'nonce': nonce,
					'order_id': order_id,
					'integration_id': this.integrationId
				};
				$('#create-document-div-' + document_type + ' input, #create-document-div-' + document_type + ' select, #create-document-div-' + document_type + ' textarea').each(function () {
					data[$(this).attr('name')] = $(this).val();
				});
				return data;
			},

			prepare_email_data: function (button_element) {
				let metadata = $(button_element).data('metadata');
				let email_class = $(button_element).data('email_class');
				let nonce = $(button_element).data('nonce');
				let order_id = $(button_element).data('order_id');
				let file_name = $(button_element).data('file_name');

				return {
					'action': this.ajaxEmailAction,
					'metadata': metadata,
					'email_class': email_class,
					'nonce': nonce,
					'order_id': order_id,
					'file_name': file_name,
					'integration_id': this.integrationId
				};
			},

			tiptip_and_date_picker: function () {
				let $tipTipElements = $('.invoice-create-button, .invoice-email-button, .invoice-link-warning');

				if (typeof $.fn.tipTip !== "undefined") {
					$tipTipElements.tipTip();
				}
				$tipTipElements.removeAttr('title');
				if (typeof $.fn.datepicker !== "undefined") {
					$('.invoice-date-picker').datepicker({dateFormat: 'yy-mm-dd'});
				}
			},

			show_hide_parameters_event: function (event) {
				let document_type = $(event.target).data('type');
				let invoice_parameters_div = $('#create-document-div-' + document_type);
				let invoice_expang_parameters_span = $(event.target).find('span.invoice-expand-parameters');
				if (invoice_parameters_div.is(':hidden')) {
					invoice_parameters_div.show();
					invoice_expang_parameters_span.html('[-]');
				} else {
					invoice_parameters_div.hide();
					invoice_expang_parameters_span.html('[+]');
				}
				$(this).blur();
				return false;
			},

			attachEvents: function () {
				let metbox_handle_name = '#' + this.metaboxId;

				$(document).on('click', metbox_handle_name + ' .invoice-create-button', this.create_invoice_event.bind(this));
				$(document).on('click', metbox_handle_name + ' .invoice-email-button', this.send_email_event.bind(this));
				$(document).on('click', metbox_handle_name + ' span.invoice-parameters a', this.show_hide_parameters_event.bind(this));

				this.tiptip_and_date_picker();
			}
		};

		$(document).ready(function () {
			let metabox_script = new MetaboxScriptClass('<?php echo $ajaxUrl; ?>', '<?php echo $ajaxAction; ?>', '<?php echo $ajaxActionEmail; ?>', '<?php echo $integrationId; ?>', '<?php echo $metaboxId; ?>');
			metabox_script.attachEvents();
		});
	})(jQuery);

</script>
