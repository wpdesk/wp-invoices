<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>
<h4 class="invoices-created"><?php echo $createdLabel; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h4>
<div class="invoices-created" id="<?php echo esc_attr( $createdDivId ); ?>">
