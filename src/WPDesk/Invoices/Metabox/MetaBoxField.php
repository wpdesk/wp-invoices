<?php

namespace WPDesk\Invoices\Metabox;

use WC_Order;
use WPDesk\Invoices\Data\OrderDefaults;
use WPDesk\Invoices\Metadata\MetadataContent;

abstract class MetaBoxField {

	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $label;

	/**
	 * MetaBoxField constructor.
	 *
	 * @param $id
	 * @param $name
	 * @param $label
	 */
	public function __construct( $id, $name, $label ) {
		$this->id    = $id;
		$this->name  = $name;
		$this->label = $label;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Prepare field.
	 *
	 * @param string $value
	 *
	 * @return array
	 */
	protected function prepareField( $value = '' ) {
		return [
			'id'    => $this->id,
			'name'  => $this->name,
			'label' => $this->label,
			'value' => $value,
		];
	}

	/**
	 * Prepare value.
	 *
	 * @param WC_Order       $order           Order.
	 * @param MetadataContent $metadataContent Meta data.
	 * @param OrderDefaults   $orderDefaults   Order defaults.
	 *
	 * @return string
	 */
	abstract protected function prepareValue(
		WC_Order $order,
		MetadataContent $metadataContent,
		OrderDefaults $orderDefaults
	);

	/**
	 * Render field.
	 *
	 * @param WC_Order       $order           Order.
	 * @param MetadataContent $metadataContent Meta data.
	 * @param OrderDefaults   $orderDefaults   Order defaults.
	 */
	public function render( WC_Order $order, MetadataContent $metadataContent, OrderDefaults $orderDefaults ) {
		woocommerce_wp_text_input( $this->prepareField( $this->prepareValue( $order, $metadataContent, $orderDefaults ) ) );
	}
}
