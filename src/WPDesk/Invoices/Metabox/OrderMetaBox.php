<?php

namespace WPDesk\Invoices\Metabox;

use Automattic\WooCommerce\Internal\DataStores\Orders\CustomOrdersTableController;
use Exception;
use WC_Order;
use WP_Post;
use WPDesk\Invoices\Ajax\AjaxCreateHandler;
use WPDesk\Invoices\Ajax\AjaxEmailHandler;
use WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use WPDesk\Invoices\Documents\Type;
use WPDesk\Invoices\Integration;
use WPDesk\Invoices\Metadata\CustomMetadata;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\PluginBuilder\Plugin\Hookable;

class OrderMetaBox implements Hookable {

	/**
	 * @var Integration
	 */
	protected $integration;

	/**
	 * @var string
	 */
	protected $metaBoxTitle;

	/**
	 * @var string
	 */
	protected $createdLabel;

	/**
	 * @var string
	 */
	protected $createdLabelNone;

	/**
	 * @var string
	 */
	protected $emailButtonAltText;

	/**
	 * @var string
	 */
	protected $confirmationMessage;

	/**
	 * @var AjaxGetPdfHandler
	 */
	protected $ajaxGetPdfHandler;

	/**
	 * @var AjaxEmailHandler
	 */
	protected $ajaxEmailHandler;

	/**
	 * @var AjaxCreateHandler
	 */
	protected $ajaxCreateHandler;

	/**
	 * OrderMetaBox constructor.
	 *
	 * @param Integration $integration
	 * @param string      $metaBoxTitle
	 * @param string      $createdLabel
	 * @param string      $createdLabelNone
	 * @param string      $emailButtonAltText
	 * @param string      $confirmationMessage
	 */
	public function __construct(
		Integration $integration,
		string $metaBoxTitle,
		string $createdLabel = 'Created',
		string $createdLabelNone = 'None',
		string $emailButtonAltText = 'Not set!',
		string $confirmationMessage = 'Document %1$s already created! Overwrite?'
	) {
		$this->integration         = $integration;
		$this->metaBoxTitle        = $metaBoxTitle;
		$this->createdLabel        = $createdLabel;
		$this->createdLabelNone    = $createdLabelNone;
		$this->emailButtonAltText  = $emailButtonAltText;
		$this->confirmationMessage = $confirmationMessage;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'add_meta_boxes', [ $this, 'addMetaBox' ], 11 );
		add_action( 'admin_head', [ $this, 'addMetaBoxScriptToHeader' ], 11 );
		add_action( 'admin_head', [ $this, 'addMetaBoxCssToHeader' ], 11 );
	}

	/**
	 * @param AjaxGetPdfHandler $ajaxGetPdfHandler
	 */
	public function setAjaxGetPdfHandler( AjaxGetPdfHandler $ajaxGetPdfHandler ) {
		$this->ajaxGetPdfHandler = $ajaxGetPdfHandler;
	}

	/**
	 * @param AjaxEmailHandler $ajaxEmailHandler
	 */
	public function setAjaxEmailHandler( AjaxEmailHandler $ajaxEmailHandler ) {
		$this->ajaxEmailHandler = $ajaxEmailHandler;
	}

	/**
	 * @param AjaxCreateHandler $ajaxCreateHandler
	 */
	public function setAjaxCreateHandler( AjaxCreateHandler $ajaxCreateHandler ) {
		$this->ajaxCreateHandler = $ajaxCreateHandler;
	}

	/**
	 * Unique metabox id (also for div#).
	 *
	 * @return string
	 */
	private function getMetaboxId(): string {
		return 'invoices_' . $this->integration->getId();
	}

	/**
	 * Ads order metabox.
	 */
	public function addMetaBox() {
		add_meta_box(
			$this->getMetaboxId(),
			$this->metaBoxTitle,
			[ $this, 'render' ],
			[ 'shop_order', 'shop_subscription', 'woocommerce_page_wc-orders' ],
			'side',
			'low'
		);
	}

	/**
	 * @throws Exception
	 */
	public function get_order_for_meta_box( $object ): WC_Order {
		if ( $object instanceof WP_Post ) {
			$order = wc_get_order( $object->ID );
			if ( $order ) {
				return $order;
			}
		} elseif ( $object instanceof WC_Order ) {
			return $object;
		}

		throw new Exception( 'Unknown order object!' );
	}

	/**
	 * Render meta box.
	 *
	 * @param $post_or_order_object
	 *
	 * @throws Exception
	 */
	public function render( $post_or_order_object ) {
		try {
			$order                  = $this->get_order_for_meta_box( $post_or_order_object );
			$supportedDocumentTypes = $this->integration->getSupportedDocumentTypes();

			$createdLabel     = $this->createdLabel;
			$createdLabelNone = $this->createdLabelNone;
			$createdDivId     = 'created-' . $this->integration->getId();

			include __DIR__ . '/views/html-metabox-created.php';

			$rendered = false;
			foreach ( $supportedDocumentTypes as $supportedDocumentType ) {
				$order_meta_datas = ( new MetadataContent( $supportedDocumentType->getMetaDataName(), $order ) )->get();

				if ( ! is_array( $order_meta_datas ) || empty( $order_meta_datas ) ) {
					continue;
				}

				foreach ( $order_meta_datas as $order_meta_data ) {
					$meta_data = new CustomMetadata( $order_meta_data, $supportedDocumentType->getMetaDataName(), $order );
					if ( $supportedDocumentType->isMetadataContentValidForDocumentType( $meta_data ) ) {
						$this->renderMetaBoxDocumentContentForOrder( $supportedDocumentType, $order, $meta_data );
						$rendered = true;
						if ( $this->integration->isDebugEnabled() ) {
							$log_name  = $supportedDocumentType->getMetaDataName() . '_log';
							$debug_log = $order->get_meta( $log_name );
							include __DIR__ . '/views/html-debug.php';
						}
					}
				}
			}

			if ( ! $rendered ) {
				include __DIR__ . '/views/html-metabox-created-none.php';
			}

			include __DIR__ . '/views/html-metabox-created-footer.php';

			foreach ( $supportedDocumentTypes as $supportedDocumentType ) {
				if ( $supportedDocumentType->isAllowedForOrder( $order ) ) {
					$metaDataContent = new MetadataContent( $supportedDocumentType->getMetaDataName(), $order );
					$this->renderMetaBoxCreateDocumentContentForOrder( $supportedDocumentType, $order, $metaDataContent );
				}
			}
		} catch ( Exception $e ) { //phpcs:ignore Generic.CodeAnalysis.EmptyStatement.DetectedCatch
			// logger
		}
	}

	/**
	 * Get metabox content.
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 * @throws Exception
	 */
	public function getContent( WC_Order $order ): string {
		ob_start();
		$this->render( $order );
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	/**
	 * Render meta box content for order. This part of metabox created document.
	 *
	 * @param Type            $documentType
	 * @param WC_Order        $order
	 * @param MetadataContent $metadataContent
	 */
	private function renderMetaBoxDocumentContentForOrder(
		Type $documentType,
		WC_Order $order,
		MetadataContent $metadataContent
	) {
		$documentMetadata = $documentType->prepareDocumentMetadata( $metadataContent );
		$typeNameLabel    = $documentMetadata->getTypeNameLabel();
		if ( $documentMetadata->isError() ) {
			if ( $documentType->isAllowedForOrder( $order ) ) {
				$errorMessage = $documentMetadata->getErrorMessage();
				include __DIR__ . '/views/html-metabox-created-document-error.php';
			}
		} else {
			$getPdfUrl           = $this->ajaxGetPdfHandler->getAjaxActionUrlForMetadata( $documentMetadata );
			$fileName            = $this->ajaxGetPdfHandler->getPdfFilenameForMetadata( $documentMetadata );
			$number              = $documentMetadata->getNumber();
			$buttonEmailLabelAlt = $this->emailButtonAltText;
			$orderId             = $order->get_id();
			$metadata            = $metadataContent->getMetaDataName();
			$emailClass          = $documentType->getEmailClass();
			$emailNonce          = wp_create_nonce( $this->ajaxEmailHandler->getAjaxActionName() . $order->get_id() . $metadata );
			if ( $documentMetadata->isWarning() ) {
				$linkClass = 'tips invoice-link-warning';
				$linkTip   = $documentMetadata->getWarningMessage();
			} else {
				$linkClass = '';
				$linkTip   = '';
			}
			include __DIR__ . '/views/html-metabox-created-document.php';
		}
	}

	/**
	 * Render meta box content for order. This part of metabox contains buttons for create document action.
	 *
	 * @param Type            $documentType
	 * @param WC_Order        $order           Order.
	 * @param MetadataContent $metadataContent Meta data.
	 */
	private function renderMetaBoxCreateDocumentContentForOrder(
		Type $documentType,
		WC_Order $order,
		MetadataContent $metadataContent
	) {
		$dataType            = $documentType->getTypeName();
		$divId               = 'invoice-document-' . $documentType->getTypeName();
		$buttonId            = 'create-document-' . $documentType->getTypeName();
		$buttonLabelAlt      = $documentType->getMetaBoxCreateButtonLabel();
		$buttonLabel         = $documentType->getMetaBoxCreateButtonLabel();
		$parametersId        = 'parameters-' . $documentType->getTypeName();
		$parmaetersLabel     = $documentType->getParametersLabel();
		$parametersDivId     = 'create-document-div-' . $documentType->getTypeName();
		$orderId             = $order->get_id();
		$nonce               = wp_create_nonce( $this->ajaxCreateHandler->getAjaxActionName() . $orderId );
		$confirmationMessage = '';
		$documentMetadata    = $documentType->prepareDocumentMetadata( $metadataContent );
		if ( ! empty( $documentMetadata->getId() ) ) {
			$confirmationMessage = sprintf( $this->confirmationMessage, $documentMetadata->getNumber() );
		}
		include __DIR__ . '/views/html-metabox-create-button.php';
		include __DIR__ . '/views/html-metabox-parameters-header.php';
		$orderDefaults = $this->integration->prepareOrderDefaults( $order, $documentType );
		foreach ( $documentType->getOrderMetaboxFields()->getMetaBoxFields() as $metaBoxField ) {
			$metaBoxField->render( $order, $metadataContent, $orderDefaults );
		}
		include __DIR__ . '/views/html-metabox-parameters-footer.php';
	}

	/**
	 * Adds meta box script to admin header.
	 */
	public function addMetaBoxScriptToHeader() {
		$ajaxUrl         = admin_url( 'admin-ajax.php' );
		$ajaxAction      = $this->ajaxCreateHandler->getAjaxActionName();
		$ajaxActionEmail = $this->ajaxEmailHandler->getAjaxActionName();
		$integrationId   = $this->integration->getId();
		$metaboxId       = $this->getMetaboxId();
		include __DIR__ . '/views/html-metabox-script.php';
	}

	/**
	 * Add MetaBox CSS to admin header.
	 */
	public function addMetaBoxCssToHeader() {
		include __DIR__ . '/views/html-metabox-css.php';
	}
}
