<?php

namespace WPDesk\Invoices\Metabox;

abstract class MetaBoxFieldDate extends MetaBoxField {

	/**
	 * Prepare field.
	 *
	 * @param string $value
	 *
	 * @return array
	 */
	protected function prepareField( $value = '' ) {
		$field                      = parent::prepareField( $value );
		$field['class']             = 'date-picker invoice-date-picker';
		$field['custom_attributes'] = [
			'pattern' => apply_filters(
				'woocommerce_date_input_html_pattern',
				'[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'
			),
		];

		return $field;
	}
}
