<?php

namespace WPDesk\Invoices\Metabox\Fields;

use WC_Order;
use WPDesk\Invoices\Data\OrderDefaults;
use WPDesk\Invoices\Metadata\MetadataContent;

/**
 * Class Amount_Field
 *
 * @package WPDesk\WooCommerceFakturownia\Metabox\Fields
 */
class AmountField extends \WPDesk\Invoices\Metabox\MetaBoxField {

	/**
	 * Prepare value.
	 *
	 * @param WC_Order                                 $order            Order.
	 * @param MetadataContent $metadata_content Meta data.
	 * @param OrderDefaults       $order_defaults   Order defaults.
	 *
	 * @return string
	 */
	protected function prepareValue(
		WC_Order $order,
		MetadataContent $metadata_content,
		OrderDefaults $order_defaults
	) {
		return $order_defaults->getDefault( 'paid_amount' );
	}
}
