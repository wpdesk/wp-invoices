<?php

namespace WPDesk\Invoices\Metabox\Fields;

use WPDesk\Invoices\Data\OrderDefaults;
use WPDesk\Invoices\Metadata\MetadataContent;

/**
 * Class PaymentMethodField
 *
 * @package WPDesk\WooCommerceFakturownia\Metabox\Fields
 */
class PaymentMethodField extends \WPDesk\Invoices\Metabox\MetaBoxFieldSelect {


	/**
	 * Prepare value.
	 *
	 * @param \WC_Order                                 $order Order.
	 * @param MetadataContent $metadata_content Meta data.
	 * @param OrderDefaults       $order_defaults Order defaults.
	 *
	 * @return string
	 */
	protected function prepareValue(
		\WC_Order $order,
		MetadataContent $metadata_content,
		OrderDefaults $order_defaults
	) {
		return $order_defaults->getDefault( 'payment_method' );
	}
}
