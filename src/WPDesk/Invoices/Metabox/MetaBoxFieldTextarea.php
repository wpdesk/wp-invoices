<?php

namespace WPDesk\Invoices\Metabox;

use WC_Order;
use WPDesk\Invoices\Data\OrderDefaults;
use WPDesk\Invoices\Metadata\MetadataContent;

abstract class MetaBoxFieldTextarea extends MetaBoxField {

	/**
	 * Render field.
	 *
	 * @param WC_Order       $order           Order.
	 * @param MetadataContent $metadataContent Meta data.
	 * @param OrderDefaults   $orderDefaults   Order defaults.
	 */
	public function render( WC_Order $order, MetadataContent $metadataContent, OrderDefaults $orderDefaults ) {
		woocommerce_wp_textarea_input(
			$this->prepareField( $this->prepareValue( $order, $metadataContent, $orderDefaults ) )
		);
	}
}
