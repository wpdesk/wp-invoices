<?php

namespace WPDesk\Invoices\Metabox;

use WC_Order;
use WPDesk\Invoices\Data\OrderDefaults;
use WPDesk\Invoices\Metadata\MetadataContent;

abstract class MetaBoxFieldSelect extends MetaBoxField {

	/**
	 * @var array
	 */
	private $options;

	/**
	 * MetaBoxField constructor.
	 *
	 * @param string $id
	 * @param string $name
	 * @param string $label
	 * @param array  $options
	 */
	public function __construct( $id, $name, $label, array $options ) {
		parent::__construct( $id, $name, $label );
		$this->options = $options;
	}

	/**
	 * @return array
	 */
	public function getOptions() {
		return $this->options;
	}

	/**
	 * @param array $options
	 */
	public function setOptions( $options ) {
		$this->options = $options;
	}

	/**
	 * Prepare field.
	 *
	 * @param string $value
	 *
	 * @return array
	 */
	protected function prepareField( $value = '' ) {
		$field            = parent::prepareField( $value );
		$field['options'] = $this->options;

		return $field;
	}

	/**
	 * Render field.
	 *
	 * @param WC_Order       $order           Order.
	 * @param MetadataContent $metadataContent Meta data.
	 * @param OrderDefaults   $orderDefaults   Order defaults.
	 */
	public function render( WC_Order $order, MetadataContent $metadataContent, OrderDefaults $orderDefaults ) {
		woocommerce_wp_select( $this->prepareField( $this->prepareValue( $order, $metadataContent, $orderDefaults ) ) );
	}
}
