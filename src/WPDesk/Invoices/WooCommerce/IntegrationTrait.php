<?php

namespace WPDesk\Invoices\WooCommerce;

use WPDesk\ApiClient\Client\Client;
use WPDesk\Invoices\Documents\Type;

trait IntegrationTrait {

	/**
	 * @var Type[];
	 */
	protected $supportedDocumentTypes = [];

	/**
	 * Api Client.
	 *
	 * @var Client
	 */
	protected $apiClient;

	/**
	 * @param Type $documentType
	 *
	 * @return mixed
	 */
	public function addSupportedDocumentType( Type $documentType ) {
		$this->supportedDocumentTypes[] = $documentType;
	}

	/**
	 * @return Type[]
	 */
	public function getSupportedDocumentTypes() {
		return $this->supportedDocumentTypes;
	}
}
