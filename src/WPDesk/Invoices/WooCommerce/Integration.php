<?php

namespace WPDesk\Invoices\WooCommerce;

use WPDesk\Invoices\Documents\Type;

interface Integration {

	/**
	 * @return void
	 */
	public function initSupportedDocumentTypes();

	/**
	 * @param Type $documentType
	 *
	 * @return mixed
	 */
	public function addSupportedDocumentType( Type $documentType );

	/**
	 * Init API Client
	 */
	public function initApiClient();
}
