<?php

namespace WPDesk\Invoices\WooCommerce;

use WPDesk\Mutex\WordpressMySQLLockMutex;
use WPDesk\PluginBuilder\Plugin\Hookable;

class DocumentCreatorForOrderStatus implements Hookable {

	const LOCKED_PREFIX = '_fakturowania_lock_';
	const LOCKED_VALUE  = 1;

	/**
	 * @var array
	 */
	public static $order_queue = [];

	/**
	 * @var \WPDesk\Invoices\Integration
	 */
	private $integration;

	/**
	 * @param \WPDesk\Invoices\Integration $integration Integration.
	 */
	public function __construct( $integration ) {
		$this->integration = $integration;
	}

	public function hooks() {
		/**
		 * Maybe issue invoice after order is created.
		 */
		add_action( 'woocommerce_new_order', [ $this, 'maybe_create_document' ], 10, 2 );

		/**
		 * Maybe issue invoice after order status has been changed
		 */
		$statuses = wc_get_order_statuses();
		foreach ( $statuses as $status_slug => $name ) {
			$status = str_replace( 'wc-', '', $status_slug );
			add_action( 'woocommerce_order_status_' . $status, [ $this, 'maybe_create_document' ], 10, 2 );
		}
	}

	/**
	 * Maybe create document.
	 *
	 * @param int       $order_id Order ID.
	 * @param \WC_Order $order    Order.
	 */
	public function maybe_create_document( $order_id, $order ) {
		$supportedDocumentTypes = $this->integration->getSupportedDocumentTypes();
		foreach ( $supportedDocumentTypes as $supportedDocumentType ) {
			$creator = $supportedDocumentType->getCreator();
			if (
				$supportedDocumentType->isAllowedForOrder( $order )
				&& $creator->isAutoCreateAllowedForOrder( $order )
			) {
				try {
					if ( $this->lock_process( $supportedDocumentType->getTypeName(), $order->get_id() ) ) {
						$creator->createDocumentForOrder( $order, [], false );
						sleep( 2 );
					}
				} catch ( \Exception $e ) { //phpcs:ignore Generic.CodeAnalysis.EmptyStatement.DetectedCatch
					// do nothing.
				} finally {
					$this->release_lock( $supportedDocumentType->getTypeName(), $order->get_id() );
				}
			}
		}
	}

	/**
	 * @param string $document_type
	 * @param        $order_id
	 *
	 * @return bool
	 */
	private function lock_process( string $document_type, $order_id ): bool {
		$name      = self::LOCKED_PREFIX . $document_type . $order_id;
		$is_locked = self::LOCKED_VALUE === (int) get_transient( $name );
		if ( false === $is_locked ) {
			set_transient( $name, self::LOCKED_VALUE, 30 );
		}

		return ! $is_locked;
	}

	/**
	 * @param string $document_type
	 * @param        $order_id
	 *
	 * @return void
	 */
	private function release_lock( string $document_type, $order_id ) {
		$name = self::LOCKED_PREFIX . $document_type . $order_id;
		delete_transient( $name );
	}
}
