<?php

namespace WPDesk\Invoices\Ajax;

use WPDesk\Invoices\Documents\Pdf;
use WPDesk\Invoices\Metabox\OrderMetaBox;
use WPDesk\Invoices\Metadata\DocumentMetadata;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Handles Ajax pdf creation/download document action.
 *
 * @package WPDesk\Invoices
 */
class AjaxGetPdfHandler extends AjaxHandler implements Hookable {

	/**
	 * @var string
	 */
	private $ajaxActionName = 'wpdesk_invoices_get_pdf';

	/**
	 * @var Pdf
	 */
	private $documentPdf;

	/**
	 * AjaxGetPdfHandler constructor.
	 *
	 * @param string       $integration_id
	 * @param OrderMetaBox $orderMetaBox
	 * @param Pdf          $documentPdf
	 */
	public function __construct( $integration_id, OrderMetaBox $orderMetaBox, Pdf $documentPdf ) {
		parent::__construct( $integration_id, $orderMetaBox );
		$this->documentPdf = $documentPdf;
	}

	/**
	 * @inheritDoc
	 */
	public function getAjaxActionName() {
		return $this->ajaxActionName;
	}

	/**
	 * Return ajax url for given metadata in context of pdf
	 *
	 * @param DocumentMetadata $metadata
	 *
	 * @return string
	 */
	public function getAjaxActionUrlForMetadata( DocumentMetadata $metadata ) {
		$documentMetadataContent = $metadata->getMetadataContent();
		$order                   = $documentMetadataContent->getOrder();
		$filename                = $this->getPdfFilenameForMetadata( $metadata );

		$getPdfUrl = parent::getAjaxActionUrl();
		$getPdfUrl = add_query_arg( 'order_id', $order->get_id(), $getPdfUrl );
		$getPdfUrl = add_query_arg( 'metadata', $documentMetadataContent->getMetaDataName(), $getPdfUrl );
		$getPdfUrl = add_query_arg( 'integration_id', $this->integration_id, $getPdfUrl );

		$getPdfUrl = add_query_arg( 'file_name', $filename, $getPdfUrl );
		$getPdfUrl = add_query_arg(
			'nonce',
			wp_create_nonce( $this->ajaxActionName . $order->get_id() . $filename ),
			$getPdfUrl
		);

		return $getPdfUrl;
	}

	/**
	 * Return pdf filename for given metadata
	 *
	 * @param DocumentMetadata $metadata
	 *
	 * @return string
	 */
	public function getPdfFilenameForMetadata( DocumentMetadata $metadata ) {
		$number = $metadata->getNumber();

		return str_replace(
			[ ' ', '/' ],
			'_',
			sprintf( '%1$s_%2$s.pdf', $metadata->getTypeName(), $number )
		);
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'wp_ajax_' . $this->ajaxActionName, [ $this, 'handleAjaxRequest' ] );
	}

	/**
	 * Handle AJAX request.
	 */
	public function handleAjaxRequest() {
		if ( ! $this->isCurrentIntegration() ) {
			return;
		}

		$response = [ self::RESPONSE_STATUS => 'error' ];
		$nonce    = $this->getRequestValue( 'nonce' );
		$orderId  = $this->getRequestValue( 'order_id' );
		$metaData = $this->getRequestValue( 'metadata' );
		$fileName = $this->getRequestValue( 'file_name', 'Document.pdf' );

		if ( wp_verify_nonce( $nonce, $this->ajaxActionName . $orderId . $fileName ) ) {
			$response[ self::RESPONSE_MESSAGE ] = 'Ups! Something go wrong!';
			try {
				$order = wc_get_order( $orderId );
				if ( $order ) {
					$metadataContent = new MetadataContent( $metaData, $order );
					$pfdContent      = $this->documentPdf->getDocumentPdf( $metadataContent );
					header( 'Content-type:application/pdf' );
					header( sprintf( 'Content-Disposition:attachment;filename=%1$s', $fileName ) );
					echo $pfdContent; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				} else {
					echo 'Order not found!';
				}
			} catch ( \Exception $e ) {
				echo $e->getMessage(); //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		} else {
			echo 'Invalid nonce!';
		}
		exit;
	}
}
