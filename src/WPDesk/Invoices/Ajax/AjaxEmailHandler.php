<?php

namespace WPDesk\Invoices\Ajax;

use Exception;
use WPDesk\Invoices\Documents\Pdf;
use WPDesk\Invoices\Metabox\OrderMetaBox;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Handles Ajax send email with document action.
 *
 * @package WPDesk\Invoices
 */
class AjaxEmailHandler extends AjaxHandler implements Hookable {

	/**
	 * @var string
	 */
	private $ajaxActionName = 'wpdesk_invoices_email';

	/**
	 * @var Pdf
	 */
	private $documentPdf;

	/**
	 * AjaxEmailHandler constructor.
	 *
	 * @param string       $integration_id
	 * @param OrderMetaBox $orderMetaBox
	 * @param Pdf          $documentPdf
	 */
	public function __construct( $integration_id, OrderMetaBox $orderMetaBox, Pdf $documentPdf ) {
		parent::__construct( $integration_id, $orderMetaBox );
		$this->documentPdf = $documentPdf;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'wp_ajax_' . $this->ajaxActionName, [ $this, 'handleAjaxRequest' ] );
	}

	/**
	 * @inheritDoc
	 */
	public function getAjaxActionName() {
		return $this->ajaxActionName;
	}

	/**
	 * Handle AJAX request.
	 */
	public function handleAjaxRequest() {
		if ( ! $this->isCurrentIntegration() ) {
			return;
		}

		$response   = [ self::RESPONSE_STATUS => 'error' ];
		$nonce      = wp_unslash( $this->getRequestValue( 'nonce' ) );
		$orderId    = wp_unslash( $this->getRequestValue( 'order_id' ) );
		$metaData   = wp_unslash( $this->getRequestValue( 'metadata' ) );
		$emailClass = wp_unslash( $this->getRequestValue( 'email_class' ) );
		$fileName   = wp_unslash( $this->getRequestValue( 'file_name', 'Document.pdf' ) );

		if ( wp_verify_nonce( $nonce, $this->ajaxActionName . $orderId . $metaData ) ) {
			$response[ self::RESPONSE_MESSAGE ] = 'Ups! Something go wrong!';
			$order                              = wc_get_order( $orderId );
			if ( $order ) {
				try {
					$metadataContent = new MetadataContent( $metaData, $order );
					$metahash        = new MetadataContent( $metaData . '_hash', $order );
					$download_url    = add_query_arg(
						[
							'order_id'         => $order->get_id(),
							'type'             => $metaData,
							'invoice_download' => $metahash->get(),
						],
						get_site_url()
					);

					$mailer = WC()->mailer();
					$emails = $mailer->get_emails();
					if ( ! empty( $emails ) && ! empty( $emails[ $emailClass ] ) ) {
						$pfdContent = $this->documentPdf->getDocumentPdf( $metadataContent );
						$emails[ $emailClass ]->trigger( $order, $pfdContent, $fileName, $download_url );
						$response[ self::RESPONSE_CONTENT ] = $this->orderMetaBox->getContent( $order );
						$response[ self::RESPONSE_STATUS ]  = 'ok';
						unset( $response[ self::RESPONSE_MESSAGE ] );
					} else {
						$response[ self::RESPONSE_MESSAGE ] = 'Email class not found!';
					}
				} catch ( Exception $e ) {
					$response[ self::RESPONSE_CONTENT ] = $this->orderMetaBox->getContent( $order );
					$response[ self::RESPONSE_MESSAGE ] = $e->getMessage();
				}
			} else {
				$response[ self::RESPONSE_MESSAGE ] = 'Order not found!';
			}
		} else {
			$response[ self::RESPONSE_MESSAGE ] = 'Invalid nonce!';
		}
		wp_send_json( $response );
	}
}
