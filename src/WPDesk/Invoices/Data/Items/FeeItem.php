<?php

namespace WPDesk\Invoices\Data\Items;

use WC_Order_Item_Fee;

class FeeItem extends InvoiceItem {

	/**
	 * LineItem constructor.
	 *
	 * @param WC_Order_Item_Fee $orderItem
	 */
	public function __construct( WC_Order_Item_Fee $orderItem ) {
		parent::__construct( $orderItem );
		$this->setQuantity( 1 );
		$this->setPriceFromOrderItem( $orderItem );
	}
}
