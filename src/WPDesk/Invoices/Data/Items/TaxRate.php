<?php

namespace WPDesk\Invoices\Data\Items;

class TaxRate {
	const INVALID_RATE_       = - 1;
	const TAX_HAS_SINGLE_RATE = 1;

	/**
	 * @var floatg
	 */
	private $rate = 0.0;

	/**
	 * @var string
	 */
	private $rate_class = '';

	/**
	 * @var int
	 */
	private $rate_id = 0;

	/**
	 * @var array
	 */
	protected $taxes;

	/**
	 * TaxRate constructor.
	 *
	 * @param array $taxes Taxes.
	 */
	public function __construct( array $taxes ) {
		$this->setRate( $taxes );
	}

	/**
	 * @param array $taxes
	 */
	private function setRate( array $taxes ) {
		$rates         = $this->getRatesDataFromTotals( $taxes );
		$has_real_rate = false;
		foreach ( $rates as $rate ) {
			if ( $rate['rate_amount'] > 0 ) {
				$this->rate_class = $rate['rate_class'];
				$this->rate       = $rate['rate_amount'];
				$this->rate_id    = $rate['rate_id'];
				$has_real_rate    = true;
			}
		}

		if ( ! $has_real_rate ) {
			foreach ( $rates as $rate ) {
				$this->rate_class = $rate['rate_class'];
				$this->rate       = $rate['rate_amount'];
				$this->rate_id    = $rate['rate_id'];
			}
		}
	}

	/**
	 * @param array $taxes
	 *
	 * @return array
	 */
	private function getRatesDataFromTotals( array $taxes ): array {
		$rates = [];

		if ( isset( $taxes['total'] ) ) {
			$total = $this->removeEmptyRates( $taxes['total'] );
			foreach ( $total as $tax_id => $tax_value ) {
				$tax_rate   = \WC_Tax::get_rate_percent( $tax_id );
				$rate       = (float) \str_replace( '%', '', $tax_rate );
				$rate_class = \wc_get_tax_class_by_tax_id( $tax_id );
				if ( ! $this->rate_class ) {
					$this->rate_class = '';
				}

				$rates[] = [
					'rate_id'     => $tax_id,
					'rate_class'  => $rate_class,
					'rate_amount' => $rate,
				];
			}
		}

		return $rates;
	}


	/**
	 * @return string
	 */
	public function getClass(): string {
		return (string) $this->rate_class;
	}

	/**
	 * @return float
	 */
	public function getRate(): float {
		return $this->rate;
	}

	/**
	 * @return int
	 */
	public function getRateId(): int {
		return $this->rate_id;
	}

	/**
	 * @param array $total
	 *
	 * @return array
	 */
	private function removeEmptyRates( array $total ): array {
		foreach ( $total as $tax_rate_id => $tax_rate ) {
			if ( $tax_rate === '' ) {
				unset( $total[ $tax_rate_id ] );
			}
		}

		return $total;
	}
}
