<?php

namespace WPDesk\Invoices\Data\Items;

class Shippingtem extends InvoiceItem {

	/**
	 * LineItem constructor.
	 *
	 * @param \WC_Order_Item_Shipping $orderItem
	 */
	public function __construct( \WC_Order_Item_Shipping $orderItem ) {
		parent::__construct( $orderItem );
		$this->setQuantity( 1 );
		$this->setPriceFromOrderItem( $orderItem );
		if ( wc_prices_include_tax() ) {
			$this->setPrice( floatval( $orderItem->get_total() ) + floatval( $orderItem->get_total_tax() ) );
		} else {
			$this->setPrice( floatval( $orderItem->get_total() ) );
		}
	}
}
