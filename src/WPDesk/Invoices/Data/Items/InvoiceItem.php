<?php

namespace WPDesk\Invoices\Data\Items;

class InvoiceItem {

	const ITEM_TYPE_LINE_ITEM = 'line_item';
	const ITEM_TYPE_FEE       = 'fee';
	const ITEM_TYPE_SHIPPING  = 'shipping';

	/**
	 * @var \WC_Order_Item
	 */
	private $orderItem;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var int
	 */
	private $quantity;

	/**
	 * @var float
	 */
	private $taxRate = 0.0;

	/**
	 * @var string
	 */
	private $taxClass = '';

	/**
	 * @var float
	 */
	protected $price;

	/**
	 * @var string
	 */
	protected $unitOfMeasure = 'pcs';

	/**
	 * @var array
	 */
	protected $additionalData = [];

	/**
	 * Create from order item.
	 *
	 * @param \WC_Order_Item $orderItem
	 * @param string         $itemNameAttribute Attribute name for item name.
	 *
	 * @return InvoiceItem
	 */
	public static function createFromOrderItem( \WC_Order_Item $orderItem, $itemNameAttribute ) {
		if ( $orderItem->is_type( self::ITEM_TYPE_FEE ) ) {
			return new FeeItem( $orderItem );
		} elseif ( $orderItem->is_type( self::ITEM_TYPE_SHIPPING ) ) {
			return new Shippingtem( $orderItem );
		} else {
			return new LineItem( $orderItem, $itemNameAttribute );
		}
	}

	/**
	 * InvoiceItem constructor.
	 *
	 * @param \WC_Order_Item $orderItem
	 */
	public function __construct( $orderItem ) {
		$this->orderItem = $orderItem;
		$this->name      = trim( $orderItem->get_name() );
		if ( method_exists( $orderItem, 'get_taxes' ) ) {
			$taxes = $orderItem->get_taxes();
			$this->setTaxRateFromTaxes( $taxes );
			$this->setTaxClassFromTaxes( $taxes );
		}
	}

	/**
	 * @return \WC_Order_Item
	 */
	public function getOrderItem() {
		return $this->orderItem;
	}

	/**
	 * @param \WC_Order_Item $orderItem
	 */
	public function setOrderItem( $orderItem ) {
		$this->orderItem = $orderItem;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity( $quantity ) {
		$this->quantity = $quantity;
	}

	/**
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice( $price ) {
		$this->price = $price;
	}

	/**
	 * @return string
	 */
	public function getUnitOfMeasure() {
		return $this->unitOfMeasure;
	}

	/**
	 * @param string $unitOfMeasure
	 */
	public function setUnitOfMeasure( $unitOfMeasure ) {
		$this->unitOfMeasure = $unitOfMeasure;
	}

	/**
	 * @return float
	 */
	public function getTaxRate() {
		return $this->taxRate;
	}

	/**
	 * @param float $taxRate
	 */
	public function setTaxRate( $taxRate ) {
		$this->taxRate = $taxRate;
	}

	/**
	 * @return string
	 */
	public function getTaxClass() {
		return $this->taxClass;
	}

	/**
	 * @param string $taxClass
	 */
	public function setTaxClass( $taxClass ) {
		$this->taxClass = $taxClass;
	}

	/**
	 * @param array $taxes
	 */
	private function setTaxRateFromTaxes( array $taxes ) {
		$taxRate       = new TaxRate( $taxes );
		$this->taxRate = $taxRate->getRate();
	}

	/**
	 * @param array $taxes
	 */
	private function setTaxClassFromTaxes( array $taxes ) {
		$taxRate        = new TaxRate( $taxes );
		$this->taxClass = $taxRate->getClass();
	}

	/**
	 * Set price from Order Item.
	 *
	 * @param \WC_Order_Item $orderItem
	 */
	protected function setPriceFromOrderItem( $orderItem ) {
		if ( wc_prices_include_tax() ) {
			$this->setPrice( floatval( $orderItem->get_total() ) + floatval( $orderItem->get_total_tax() ) );
		} else {
			$this->setPrice( floatval( $orderItem->get_total() ) );
		}
	}

	/**
	 * @param string $name
	 * @param mixed  $value
	 */
	public function addAdditionalData( $name, $value ) {
		$this->additionalData[ $name ] = $value;
	}

	/**
	 * @param string $name
	 *
	 * @return mixed|null
	 */
	public function getAdditionalDataByName( $name ) {
		if ( isset( $this->additionalData[ $name ] ) ) {
			return $this->additionalData[ $name ];
		}

		return null;
	}
}
