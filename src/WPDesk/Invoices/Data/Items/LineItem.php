<?php

namespace WPDesk\Invoices\Data\Items;

class LineItem extends InvoiceItem {

	/**
	 * LineItem constructor.
	 *
	 * @param \WC_Order_Item_Product $orderItem
	 * @param string                 $itemNameAttribute Attribute name for item name.
	 */
	public function __construct( \WC_Order_Item_Product $orderItem, $itemNameAttribute ) {
		parent::__construct( $orderItem );
		$this->setQuantity( $orderItem->get_quantity() );
		$this->setPriceFromOrderItem( $orderItem );
		if ( wc_prices_include_tax() ) {
			$this->setPrice( floatval( $orderItem->get_total() ) + floatval( $orderItem->get_total_tax() ) );
		} else {
			$this->setPrice( floatval( $orderItem->get_total() ) );
		}
		$this->setPrice( $this->getPrice() / $this->getQuantity() );
		if ( ! empty( $itemNameAttribute ) ) {
			$product = $orderItem->get_product();
			if ( $product instanceof \WC_Product ) {
				if ( $product->is_type( 'variation' ) ) {
					$parent   = wc_get_product( $product->get_parent_id() );
					$itemName = $parent->get_attribute( $itemNameAttribute );
					if ( ! empty( $itemName ) ) {
						$this->setName( $itemName );
					}
				} else {
					$itemName = $product->get_attribute( $itemNameAttribute );
					if ( ! empty( $itemName ) ) {
						$this->setName( $itemName );
					}
				}
			}
		}
	}
}
