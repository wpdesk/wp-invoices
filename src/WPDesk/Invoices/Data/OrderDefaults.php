<?php

namespace WPDesk\Invoices\Data;

use WPDesk\Invoices\Documents\Type;

class OrderDefaults {

	/**
	 * @var \WC_Order
	 */
	private $order;

	/**
	 * @var array
	 */
	private $defaults = [];

	/**
	 * @var Type
	 */
	private $documentType;

	/**
	 * OrderDefaults constructor.
	 *
	 * @param \WC_Order $order
	 * @param Type      $documentType
	 */
	public function __construct( \WC_Order $order, Type $documentType ) {
		$this->order        = $order;
		$this->documentType = $documentType;
	}

	/**
	 * @return array
	 */
	public function getDefaults() {
		return $this->defaults;
	}

	/**
	 * @param string $name
	 * @param string $default
	 *
	 * @return string
	 */
	public function getDefault( $name, $default = '' ) {
		if ( isset( $this->defaults[ $name ] ) ) {
			return $this->defaults[ $name ];
		}

		return $default;
	}

	/**
	 * @param string $name
	 * @param        $value
	 */
	protected function addDefault( $name, $value ) {
		$this->defaults[ $name ] = $value;
	}

	/**
	 * @param array $data
	 */
	public function setFromData( array $data ) {
		$this->defaults = array_merge( $this->defaults, $data );
	}
}
