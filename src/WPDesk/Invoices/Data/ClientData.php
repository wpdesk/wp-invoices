<?php

namespace WPDesk\Invoices\Data;

use WPDesk\Invoices\Field\VatNumber;

/**
 * Class ClientData
 *
 * @package WPDesk\Invoices\Data
 */
class ClientData {

	/**
	 * @var string
	 */
	private $firstName;

	/**
	 * @var string
	 */
	private $lastName;

	/**
	 * @var string
	 */
	private $companyName;

	/**
	 * @var string
	 */
	private $address;

	/**
	 * @var string
	 */
	private $address2;

	/**
	 * @var string
	 */
	private $postCode;

	/**
	 * @var string
	 */
	private $city;

	/**
	 * @var string
	 */
	private $country;

	/**
	 * @var string
	 */
	private $vatNumber;

	/**
	 * @var string
	 */
	private $phone;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * Create from order.
	 *
	 * @param \WC_Order      $order     Order.
	 * @param VatNumber|null $vatNumber Vat Number field.
	 *
	 * @return ClientData
	 */
	public static function createFromOrder( \WC_Order $order, $vatNumber ) {
		$clientData = new self();
		$clientData->setCompanyName( $order->get_billing_company() );
		$clientData->setFirstName( $order->get_billing_first_name() );
		$clientData->setLastName( $order->get_billing_last_name() );
		$clientData->setAddress( $order->get_billing_address_1() );
		$clientData->setAddress2( $order->get_billing_address_2() );
		$clientData->setPostCode( $order->get_billing_postcode() );
		$clientData->setCity( $order->get_billing_city() );
		$clientData->setCountry( $order->get_billing_country() );
		$clientData->setEmail( $order->get_billing_email() );
		$clientData->setPhone( $order->get_billing_phone() );

		if ( null !== $vatNumber ) {
			$clientData->setVatNumber( $vatNumber->getFromOrder( $order ) );
		}

		return $clientData;
	}

	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 */
	private function setFirstName( $firstName ) {
		$this->firstName = $firstName;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	private function setLastName( $lastName ) {
		$this->lastName = $lastName;
	}

	/**
	 * @return string
	 */
	public function getCompanyName() {
		return $this->companyName;
	}

	/**
	 * @param string $companyName
	 */
	private function setCompanyName( $companyName ) {
		$this->companyName = $companyName;
	}

	/**
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	private function setAddress( $address ) {
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getAddress2() {
		return $this->address2;
	}

	/**
	 * @param string $address2
	 */
	private function setAddress2( $address2 ) {
		$this->address2 = $address2;
	}

	/**
	 * @return string
	 */
	public function getPostCode() {
		return $this->postCode;
	}

	/**
	 * @param string $postCode
	 */
	private function setPostCode( $postCode ) {
		$this->postCode = $postCode;
	}

	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	private function setCity( $city ) {
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	private function setCountry( $country ) {
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getVatNumber() {
		return $this->vatNumber;
	}

	/**
	 * @param string $vatNumber
	 */
	private function setVatNumber( $vatNumber ) {
		$this->vatNumber = $vatNumber;
	}

	/**
	 * @return bool
	 */
	public function isComapny() {
		return ! empty( $this->companyName );
	}

	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	private function setPhone( $phone ) {
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	private function setEmail( $email ) {
		$this->email = $email;
	}
}
