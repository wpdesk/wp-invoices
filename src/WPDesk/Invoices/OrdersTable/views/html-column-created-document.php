<?php

use WPDesk\Invoices\Metadata\DocumentMetadata;

/**
 * @var string           $typeNameLabel
 * @var string           $number
 * @var string           $getPdfUrl
 * @var string           $linkClass
 * @var string           $linkTip
 * @var DocumentMetadata $documentMetadata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>
<div>
	<strong><?php echo esc_html( $typeNameLabel ); ?>:</strong>
	<?php if ( $documentMetadata->getId() ) : ?>
		<a
			href="<?php echo esc_attr( $getPdfUrl ); ?>"
			class="<?php echo esc_attr( $linkClass ); ?>"
			title="<?php echo esc_attr( $linkTip ); ?>"
			data-tip="<?php echo esc_attr( $linkTip ); ?>"
			target="_blank"
		><?php echo esc_html( $number ); ?></a>
	<?php else : ?>
		<?php echo esc_html( $number ); ?>
	<?php endif; ?>
</div>
