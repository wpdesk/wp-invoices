<?php
/**
 * @var string $label
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?><div>
	<?php echo esc_html( $label ); ?>
</div>
