<?php

namespace WPDesk\Invoices\OrdersTable;

use WC_Order;
use WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use WPDesk\Invoices\Documents\Type;
use WPDesk\Invoices\Field\InvoiceAsk;
use WPDesk\Invoices\Integration;
use WPDesk\Invoices\Metadata\CustomMetadata;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\PluginBuilder\Plugin\Hookable;

class OrderColumn implements Hookable {

	/**
	 * @var Integration
	 */
	protected $integration;

	/**
	 * @var string
	 */
	protected $columnTitle;

	/**
	 * @var InvoiceAsk
	 */
	protected $invoiceAskField;

	/**
	 * @var AjaxGetPdfHandler
	 */
	protected $ajaxGetPdfHandler;

	/**
	 * OrderMetaBox constructor.
	 *
	 * @param Integration       $integration
	 * @param string            $columnTitle
	 * @param InvoiceAsk|null   $invoiceAskField
	 * @param AjaxGetPdfHandler $ajaxGetPdfHandler
	 */
	public function __construct(
		Integration $integration,
		$columnTitle,
		$invoiceAskField,
		AjaxGetPdfHandler $ajaxGetPdfHandler
	) {
		$this->integration       = $integration;
		$this->columnTitle       = $columnTitle;
		$this->invoiceAskField   = $invoiceAskField;
		$this->ajaxGetPdfHandler = $ajaxGetPdfHandler;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'manage_edit-shop_order_columns', [ $this, 'orderColumns' ], 11 );
		add_action( 'manage_shop_order_posts_custom_column', [ $this, 'orderColumnContent' ], 11, 2 );
		add_filter( 'manage_woocommerce_page_wc-orders_columns', [ $this, 'orderColumns' ] );
		add_action( 'manage_woocommerce_page_wc-orders_custom_column', [ $this, 'orderColumnContent' ], 10, 2 );
	}

	/**
	 * Add orders column.
	 *
	 * @param array $columns Columns.
	 *
	 * @return array
	 */
	public function orderColumns( array $columns ) {
		$cols2 = [];
		foreach ( $columns as $key => $title ) {
			if ( $key === 'order_actions' ) {
				$cols2[ $this->integration->getId() ] = $this->columnTitle;
			}
			$cols2[ $key ] = $title;
		}
		if ( empty( $cols2[ $this->integration->getId() ] ) ) {
			$cols2[ $this->integration->getId() ] = $this->columnTitle;
		}

		return $cols2;
	}

	/**
	 * @throws \Exception
	 */
	public function get_order_for_meta_box( $object ): WC_Order {
		if ( $object instanceof \WP_Post ) {
			$order = wc_get_order( $object->ID );
			if ( $order ) {
				return $order;
			}
		} elseif ( $object instanceof WC_Order ) {
			return $object;
		}

		throw new \Exception( 'Unknown order object!' );
	}

	/**
	 * Display column content.
	 *
	 * @param string $column Column.
	 */
	public function orderColumnContent( $column, $order_id ) {
		if ( $column === $this->integration->getId() ) {
			$order             = wc_get_order( $order_id );
			$documentsRendered = $this->renderDocumentsContent( $order );
			if ( $documentsRendered === 0 && null != $this->invoiceAskField ) { //phpcs:ignore Universal.Operators.StrictComparisons.LooseNotEqual
				$this->maybeRenderInvoiceAskField( $order );
			}
		}
	}

	/**
	 * Maybe render invoice ask field.
	 *
	 * @param WC_Order $order
	 */
	private function maybeRenderInvoiceAskField( $order ) {
		$invoiceAskFieldValue = $this->invoiceAskField->getFromOrder( $order );
		if ( $invoiceAskFieldValue ) {
			$label = $this->invoiceAskField->getLabel();
			include __DIR__ . '/views/html-column-created-document-invoice-ask.php';
		}
	}

	/**
	 * Render documents for order.
	 *
	 * @param WC_Order $order
	 *
	 * @return int Rendered documents count.
	 */
	private function renderDocumentsContent( $order ) {
		$supportedDocumentTypes = $this->integration->getSupportedDocumentTypes();
		$documentsRendered      = 0;
		foreach ( $supportedDocumentTypes as $supportedDocumentType ) {
			$order_meta_data = new MetadataContent( $supportedDocumentType->getMetaDataName(), $order );
			$metas           = $order_meta_data->get();
			if ( ! is_array( $metas ) || empty( $metas ) ) {
				continue;
			}
			foreach ( $metas as $meta ) {
				$metaDataContent = new CustomMetadata( $meta, $supportedDocumentType->getMetaDataName(), $order );
				if ( $supportedDocumentType->isMetadataContentValidForDocumentType( $metaDataContent ) ) {
					$this->renderColumnDocumentContentForOrder( $supportedDocumentType, $order, $metaDataContent );
					++$documentsRendered;
				}
			}
		}

		return $documentsRendered;
	}

	/**
	 * Render column document content.
	 *
	 * @param Type            $documentType
	 * @param WC_Order        $order
	 * @param MetadataContent $metadataContent
	 */
	public function renderColumnDocumentContentForOrder(
		Type $documentType,
		WC_Order $order,
		MetadataContent $metadataContent
	) {
		$documentMetadata = $documentType->prepareDocumentMetadata( $metadataContent );
		$typeNameLabel    = $documentMetadata->getTypeNameLabel();
		if ( $documentMetadata->isError() ) {
			if ( $documentType->isAllowedForOrder( $order ) ) {
				$errorMessage = $documentMetadata->getErrorMessage();
				include __DIR__ . '/views/html-column-created-document-error.php';
			}
		} else {
			$getPdfUrl = $this->ajaxGetPdfHandler->getAjaxActionUrlForMetadata( $documentMetadata );
			$fileName  = $this->ajaxGetPdfHandler->getPdfFilenameForMetadata( $documentMetadata );
			$number    = $documentMetadata->getNumber();

			if ( $documentMetadata->isWarning() ) {
				$linkClass = 'tips invoice-link-warning';
				$linkTip   = $documentMetadata->getWarningMessage();
			} else {
				$linkClass = '';
				$linkTip   = '';
			}
			include __DIR__ . '/views/html-column-created-document.php';
		}
	}
}
