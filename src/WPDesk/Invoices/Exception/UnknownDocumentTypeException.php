<?php

namespace WPDesk\Invoices\Exception;

class UnknownDocumentTypeException extends \RuntimeException {

	public function __construct( $documentType ) {
		$message = sprintf( 'Unknown document type %1$s!', $documentType );
		parent::__construct( $message );
	}
}
