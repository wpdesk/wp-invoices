<?php

namespace WPDesk\Invoices\Exception;

class DocumentAlreadyExistsException extends \RuntimeException {

	/**
	 * DocumentAlreadyExistsException constructor.
	 */
	public function __construct() {
		parent::__construct( 'Document already exists!' );
	}
}
