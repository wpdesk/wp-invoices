<?php

namespace WPDesk\Invoices;

use WPDesk\ApiClient\Client\Client;
use WPDesk\Invoices\Ajax\AjaxCreateHandler;
use WPDesk\Invoices\Ajax\AjaxEmailHandler;
use WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use WPDesk\Invoices\Data\OrderDefaults;
use WPDesk\Invoices\Documents\AbstractCreator;
use WPDesk\Invoices\Documents\Creator;
use WPDesk\Invoices\Documents\Pdf;
use WPDesk\Invoices\Documents\Type;
use WPDesk\Invoices\Field\FormField;
use WPDesk\Invoices\Metabox\OrderMetaBox;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\Invoices\Order\MyAccount;
use WPDesk\Invoices\OrdersTable\OrderColumn;
use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\PluginBuilder\Plugin\HookableCollection;
use WPDesk\PluginBuilder\Plugin\HookableParent;

abstract class Integration implements Hookable, HookableCollection, Pdf, Creator {

	use HookableParent;

	/**
	 * @var Type[];
	 */
	protected $supportedDocumentTypes = [];

	/**
	 * Api Client.
	 *
	 * @var Client
	 */
	protected $apiClient;

	/**
	 * @var string
	 */
	protected $metaBoxTitle;

	/**
	 * @var string
	 */
	protected $id = 'invoices';

	/**
	 * @var OrderMetaBox
	 */
	private $orderMetaBox;

	/**
	 * @var AjaxGetPdfHandler
	 */
	private $ajaxGetPdfHandler;

	/**
	 * @var AjaxCreateHandler
	 */
	private $ajaxCreateHandler;

	/**
	 * @var AjaxEmailHandler
	 */
	private $ajaxEmailHandler;

	/**
	 * Integration constructor.
	 *
	 * @TODO: give some method to override/decorate classes in hookable in child classes
	 *
	 * @param string $id           Integration ID.
	 * @param string $metaboxTitle Meta box title.
	 * @param string $createdLabel
	 * @param string $createdLabelNone
	 * @param string $emailButtonAltText
	 * @param string $confirmationMessage
	 */
	public function __construct(
		$id,
		$metaboxTitle = 'Invoices',
		$createdLabel = 'Created',
		$createdLabelNone = 'None',
		$emailButtonAltText = 'None',
		$confirmationMessage = 'Document %1$s already created! Overwrite?'
	) {
		$this->id           = $id;
		$this->metaBoxTitle = $metaboxTitle;
		$this->initApiClient();
		$this->orderMetaBox = new OrderMetaBox(
			$this,
			$metaboxTitle,
			$createdLabel,
			$createdLabelNone,
			$emailButtonAltText,
			$confirmationMessage
		);
		$this->add_hookable( $this->orderMetaBox );
		$this->ajaxCreateHandler = new AjaxCreateHandler( $this->id, $this->orderMetaBox, $this );
		$this->add_hookable( $this->ajaxCreateHandler );
		$this->ajaxGetPdfHandler = new AjaxGetPdfHandler( $this->id, $this->orderMetaBox, $this );
		$this->add_hookable( $this->ajaxGetPdfHandler );
		$this->ajaxEmailHandler = new AjaxEmailHandler( $this->id, $this->orderMetaBox, $this );
		$this->add_hookable( $this->ajaxEmailHandler );

		$this->orderMetaBox->setAjaxCreateHandler( $this->ajaxCreateHandler );
		$this->orderMetaBox->setAjaxEmailHandler( $this->ajaxEmailHandler );
		$this->orderMetaBox->setAjaxGetPdfHandler( $this->ajaxGetPdfHandler );

		$this->initFields();
		$this->initOrderColumn();
		$this->initSupportedDocumentTypes();
		$this->initDocumentCreators();
		$this->initIntegrations();

		$this->add_hookable( new MyAccount( $this->getSupportedDocumentTypes() ) );
	}

	/**
	 * @return void
	 */
	public function hooks() {
		$this->hooks_on_hookable_objects();
	}

	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return AjaxGetPdfHandler
	 */
	public function getAjaxGetPdfHandler() {
		return $this->ajaxGetPdfHandler;
	}

	/**
	 * @return Type[]
	 */
	abstract public function initSupportedDocumentTypes();

	/**
	 * @param Type $documentType
	 *
	 * @return void
	 */
	public function addSupportedDocumentType( $documentType ) {
		$this->supportedDocumentTypes[ $documentType->getTypeName() ] = $documentType;
	}

	/**
	 * @return Type[]
	 */
	public function getSupportedDocumentTypes() {
		return $this->supportedDocumentTypes;
	}

	/**
	 * @param $typeName
	 *
	 * @return Type|bool
	 */
	public function getSupportedDocumentType( $typeName ) {
		if ( isset( $this->supportedDocumentTypes[ $typeName ] ) ) {
			return $this->supportedDocumentTypes[ $typeName ];
		}

		return false;
	}

	/**
	 * @return Client
	 */
	public function getApiClient() {
		return $this->apiClient;
	}

	/**
	 * @param Client $apiClient
	 */
	public function setApiClient( $apiClient ) {
		$this->apiClient = $apiClient;
	}

	/**
	 * Init API Client
	 *
	 * @return Client
	 */
	abstract public function initApiClient();

	/**
	 * Inits checkout fields.
	 *
	 * @return FormField[]
	 */
	abstract public function initFields();

	/**
	 * Inits order column.
	 *
	 * @return OrderColumn
	 */
	abstract public function initOrderColumn();

	/**
	 * Inits document creators.
	 *
	 * @return AbstractCreator[]
	 */
	abstract public function initDocumentCreators();

	/**
	 * Inits integrations.
	 *
	 * @return Hookable[]
	 */
	abstract public function initIntegrations();

	/**
	 * @param \WC_Order $order
	 * @param Type      $documentType
	 *
	 * @return OrderDefaults
	 */
	abstract public function prepareOrderDefaults( $order, Type $documentType );

	/**
	 * Create document for order.
	 *
	 * @param \WC_Order $order             Order.
	 * @param array     $data              Posted data.
	 * @param bool      $overwriteExisting Overwrite already created.
	 *
	 * @throws \Exception
	 */
	abstract public function createDocumentForOrder( $order, array $data, $overwriteExisting = true );

	/**
	 * Get Document PDF.
	 *
	 * @param MetadataContent $metadataContent
	 *
	 * @return string
	 */
	abstract public function getDocumentPdf( MetadataContent $metadataContent );

	/**
	 * @return bool
	 */
	public function isDebugEnabled() {
		return false;
	}
}
