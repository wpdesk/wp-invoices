<?php

namespace WPDesk\Invoices\Metadata;

class CustomMetadata extends MetadataContent {

	private array $custom_meta_data;

	public function __construct( array $custom_meta_data, $metaDataName, \WC_Order $order ) {
		$this->metaDataName     = $metaDataName;
		$this->custom_meta_data = $custom_meta_data;
		$this->order            = $order;
		parent::__construct( $metaDataName, $order );
	}

	public function get() {
		return $this->custom_meta_data ?? [];
	}

	public function update( $metaData, $save = true ) {
		$this->custom_meta_data = $metaData;
	}
}
