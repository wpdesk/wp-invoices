<?php

namespace WPDesk\Invoices\Metadata;

class MetadataContent {

	/**
	 * @var string
	 */
	protected $metaDataName;

	/**
	 * @var \WC_Order
	 */
	protected $order;

	public function __construct( $metaDataName, \WC_Order $order ) {
		$this->metaDataName = $metaDataName;
		$this->order        = $order;
	}

	/**
	 * @return \WC_Order
	 */
	public function getOrder() {
		return $this->order;
	}
	/**
	 * @return array
	 */
	public function get() {
		$meta_data = $this->order->get_meta( $this->metaDataName );
		if ( is_array( $meta_data ) ) {
			if ( array_is_list( $meta_data ) === false ) {
				return [ $meta_data ];
			}

			return $this->filter_errors_except_last( $meta_data );
		}

		return $meta_data;
	}

	/**
	 * Remove positions with 'error' as a key except the last one so the consumer can see the error.
	 *
	 * @param array $metadatas
	 *
	 * @return array
	 */
	private function filter_errors_except_last( array $metadatas ) {
		$last_key = array_key_last( $metadatas );
		foreach ( $metadatas as $key => $metadata ) {
			if ( $key !== $last_key && isset( $metadata['error'] ) ) {
				unset( $metadatas[ $key ] );
			}
		}

		return $metadatas;
	}

	/**
	 * @param array $metaData
	 * @param bool $save
	 */
	public function update( $metaData, $save = true ) {
		$this->order->update_meta_data( $this->metaDataName, $metaData );
		if ( $save ) {
			$this->order->save();
		}
	}

	/**
	 * @return string
	 */
	public function getMetaDataName() {
		return $this->metaDataName;
	}
}
