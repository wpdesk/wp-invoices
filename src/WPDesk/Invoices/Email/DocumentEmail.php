<?php

namespace WPDesk\Invoices\Email;

class DocumentEmail extends \WC_Email {

	/**
	 * @var string
	 */
	private $templatePath = '';

	/**
	 * @var string
	 */
	protected $download_url;

	/**
	 * @param $templatePath
	 */
	public function setTemplatePath( $templatePath ) {
		$this->templatePath = $templatePath;
	}

	/**
	 * Get content HTML.
	 *
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html(
			$this->template_html,
			[
				'order'         => $this->object,
				'download_url'  => $this->download_url,
				'email_heading' => $this->get_heading(),
				'sent_to_admin' => false,
				'plain_text'    => false,
				'email'         => $this->recipient,
				'email_object'  => $this,
			],
			'',
			$this->template_base
		);
	}

	/**
	 * Get_content_plain function.
	 *
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html(
			$this->template_plain,
			[
				'order'         => $this->object,
				'download_url'  => $this->download_url,
				'email_heading' => $this->get_heading(),
				'sent_to_admin' => false,
				'plain_text'    => true,
				'email'         => $this->recipient,
				'email_object'  => $this,
			],
			'',
			$this->template_base
		);
	}

	/**
	 * Initialise Settings Form Fields
	 *
	 * @return void
	 */
	public function init_form_fields() {
		$this->form_fields = [
			'subject'    => [
				'title'       => __( 'Subject', 'woocommerce' ),
				'type'        => 'text',
				'placeholder' => $this->subject,
				'default'     => '',
			],
			'heading'    => [
				'title'       => __( 'Email Heading', 'woocommerce' ),
				'type'        => 'text',
				'placeholder' => $this->heading,
				'default'     => '',
			],
			'email_type' => [
				'title'       => __( 'Email type', 'woocommerce' ),
				'type'        => 'select',
				'description' => __( 'Choose which format of email to send.', 'woocommerce' ),
				'default'     => 'html',
				'class'       => 'email_type',
				'options'     => [
					'plain'     => __( 'Plain text', 'woocommerce' ),
					'html'      => __( 'HTML', 'woocommerce' ),
					'multipart' => __( 'Multipart', 'woocommerce' ),
				],
			],
		];
	}

	/**
	 * @param \WC_Order $order
	 * @param string    $pdfDocument
	 * @param string    $fileName
	 * @param string    $download_url
	 */
	public function trigger( $order, $pdfDocument, $fileName, $download_url ) {
		if ( ! is_object( $order ) ) {
			$order = new \WC_Order( absint( $order ) );
		}

		if ( $order ) {
			$this->object                         = $order;
			$this->recipient                      = $order->get_billing_email();
			$this->download_url                   = $download_url;
			$this->placeholders['{order_date}']   = date_i18n(
				wc_date_format(),
				$order->get_date_created() ? $order->get_date_created()->getTimestamp() : strtotime( current_time( 'mysql' ) )
			);
			$this->placeholders['{order_number}'] = $order->get_order_number();
		}

		if ( ! $this->get_recipient() ) {
			return;
		}

		$attachments       = [
			'pdf' => [
				'fileName' => $fileName,
				'content'  => $pdfDocument,
			],
		];
		$stringAttachments = new StringAttachments( $attachments );

		$stringAttachments->addAction();
		$this->send(
			$this->get_recipient(),
			$this->get_subject(),
			$this->get_content(),
			$this->get_headers(),
			[]
		);
		$stringAttachments->removeAction();
	}
}
