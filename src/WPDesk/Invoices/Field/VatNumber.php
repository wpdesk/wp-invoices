<?php


namespace WPDesk\Invoices\Field;

class VatNumber extends FormField {

	/**
	 * Field label.
	 *
	 * @var string
	 */
	protected $label;

	/**
	 * Field placeholder.
	 *
	 * @var string
	 */
	protected $placeholder;

	/**
	 * FormField constructor.
	 *
	 * @param string $fieldId     Field ID.
	 * @param string $label       Label.
	 * @param string $placeholder Placeholder.
	 */
	public function __construct( $fieldId, $label, $placeholder ) {
		parent::__construct( $fieldId );
		$this->label       = $label;
		$this->placeholder = $placeholder;
	}

	/**
	 * @param array $fields
	 * @param array $args
	 *
	 * @return array
	 */
	public function addAddressReplacements( $fields, $args ) {
		if ( ! empty( $args[ $this->getFieldId() ] ) ) {
			$fields[ '{' . $this->getFieldId() . '}' ] = $this->label . ': ' . $args[ $this->getFieldId() ];
		} else {
			$fields[ '{' . $this->getFieldId() . '}' ] = '';
		}

		return $fields;
	}

	/**
	 * Prepare checkout field.
	 *
	 * @param null|int $field_priority Field priority
	 *
	 * @return bool|array
	 */
	protected function prepareCheckoutField( $field_priority = null ) {
		return [
			'label'       => $this->label,
			'placeholder' => $this->placeholder,
			'required'    => false,
			'class'       => is_admin() ? '' : [ 'form-row-wide' ],
			'clear'       => true,
			'priority'    => $field_priority,
		];
	}

	/**
	 * Prepare admin field.
	 *
	 * @return bool|array
	 */
	protected function prepareAdminField() {
		$field         = $this->prepareCheckoutField();
		$field['show'] = false;

		return $field;
	}
}
