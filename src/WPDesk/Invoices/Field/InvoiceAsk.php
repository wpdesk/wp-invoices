<?php

namespace WPDesk\Invoices\Field;

class InvoiceAsk extends FormField {
	const CHECKBOX_CHECKED_VALUE   = '1';
	const CHECKBOX_UNCHECKED_VALUE = '0';

	/**
	 * Field label.
	 *
	 * @var string
	 */
	protected $label;

	/**
	 * FormField constructor.
	 *
	 * @param string $fieldId Field ID.
	 * @param string $label   Label.
	 */
	public function __construct( $fieldId, $label ) {
		parent::__construct( $fieldId );
		$this->label = $label;
	}

	/**
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * @param array $args
	 *
	 * @return bool
	 */
	private function isFieldCheckedFromArgs( array $args ) {
		if ( ! empty( $args[ $this->getFieldId() ] ) && strval( $args[ $this->getFieldId() ] ) === self::CHECKBOX_CHECKED_VALUE ) {
			return true;
		}

		return false;
	}

	/**
	 * @param array $fields
	 * @param array $args
	 *
	 * @return array
	 */
	public function addAddressReplacements( $fields, $args ) {
		if ( $this->isFieldCheckedFromArgs( $args ) ) {
			$fields[ '{' . $this->getFieldId() . '}' ] = $this->label;
		} else {
			$fields[ '{' . $this->getFieldId() . '}' ] = '';
		}

		return $fields;
	}

	/**
	 * Prepare checkout field.
	 *
	 * @param null|int $field_priority Field priority
	 *
	 * @return bool|array
	 */
	protected function prepareCheckoutField( $field_priority = null ) {
		return [
			'label'    => $this->label,
			'required' => false,
			'class'    => [ 'form-row-wide' ],
			'type'     => 'checkbox',
			'clear'    => true,
			'priority' => $field_priority,
		];
	}

	/**
	 * Prepare admin field.
	 *
	 * @return bool|array
	 */
	protected function prepareAdminField() {
		$field = [
			'label'    => $this->label,
			'required' => false,
			'class'    => 'form-row-wide',
			'type'     => 'select',
			'clear'    => true,
			'options'  => [
				self::CHECKBOX_UNCHECKED_VALUE => __( 'No', 'woocommerce' ),
				self::CHECKBOX_CHECKED_VALUE   => __( 'Yes', 'woocommerce' ),
			],
			'show'     => false,
		];

		return $field;
	}
}
