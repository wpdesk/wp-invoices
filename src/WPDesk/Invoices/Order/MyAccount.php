<?php

namespace WPDesk\Invoices\Order;

use WPDesk\Invoices\Documents\Type;
use WPDesk\Invoices\Metadata\CustomMetadata;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Display documents on my account.
 */
class MyAccount implements Hookable {

	/**
	 * @var Type[]
	 */
	protected $supported_document_types;

	/**
	 * MyAccount constructor.
	 *
	 * @param Type[] $supported_document_types .
	 */
	public function __construct(
		$supported_document_types
	) {
		$this->supported_document_types = $supported_document_types;
	}

	/**
	 * @return void|null
	 */
	public function hooks() {
		add_action( 'woocommerce_view_order', [ $this, 'viewDocuments' ] );
	}

	/**
	 * @param Type      $supportedDocumentType
	 * @param \WC_Order $order
	 */
	private function maybeViewDocumentForType( Type $supportedDocumentType, $order ) {

		$metaDataContent = new MetadataContent( $supportedDocumentType->getMetaDataName(), $order );
		$meta            = $metaDataContent->get();
		if ( is_array( $meta ) ) {
			$metaContent = end( $meta );
			$custom_meta = new CustomMetadata( $metaContent, $supportedDocumentType->getMetaDataName(), $order );
		} else {
			$custom_meta = $metaDataContent;
		}

		if ( $supportedDocumentType->isMetadataContentValidForDocumentType( $custom_meta ) ) {
			if ( ! empty( $supportedDocumentType->getDocumentView() ) ) {
				$supportedDocumentType->getDocumentView()->render( $supportedDocumentType, $order, $custom_meta );
			}
		}
	}

	/**
	 * @param int $order_id
	 */
	public function viewDocuments( $order_id ) {
		$order = wc_get_order( $order_id );
		foreach ( $this->supported_document_types as $supportedDocumentType ) {
			$this->maybeViewDocumentForType( $supportedDocumentType, $order );
		}
	}
}
