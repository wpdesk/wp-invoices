<?php

namespace WPDesk\Invoices\Order;

use WPDesk\Invoices\Documents\Type;
use WPDesk\Invoices\Metadata\MetadataContent;

/**
 * Display document on MyAccount order view.
 */
abstract class DocumentView {

	/**
	 * Render.
	 *
	 * @param Type            $documentType
	 * @param \WC_Order       $order
	 * @param MetadataContent $metadataContent
	 */
	abstract public function render(
		Type $documentType,
		$order,
		MetadataContent $metadataContent
	);
}
