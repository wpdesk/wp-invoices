<?php

namespace WPDesk\Invoices\Order;

use WC_Order;
use WPDesk\Invoices\Ajax\AjaxGetPdfHandler;
use WPDesk\Invoices\Documents\Type;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\View\Renderer\Renderer;

/**
 * That class is for rendering document
 *
 * @package WPDesk\WooCommerceFakturownia\Metadata
 */
class OrderDocumentView extends DocumentView {

	/**
	 * @var Renderer
	 */
	private $renderer;

	/** @var AjaxGetPdfHandler */
	private $ajaxHandler;

	public function __construct( AjaxGetPdfHandler $ajaxHandler, Renderer $renderer ) {
		$this->ajaxHandler = $ajaxHandler;
		$this->renderer    = $renderer;
	}

	/**
	 * Render.
	 *
	 * @param Type            $document_type    Document type.
	 * @param WC_Order        $order            Order.
	 * @param MetadataContent $metadata_content Metadata content.
	 */
	public function render(
		Type $document_type,
		$order,
		MetadataContent $metadata_content
	) {
		$document_metadata = $document_type->prepareDocumentMetadata( $metadata_content );
		$type_name_label   = $document_metadata->getTypeNameLabel();
		if ( ! $document_metadata->isError() & $document_type->show_document_in_my_account() ) {
			$getPdfUrl = $this->ajaxHandler->getAjaxActionUrlForMetadata( $document_metadata );
			$number    = $document_metadata->getNumber();

            //phpcs:disable
            //Output should be escaped in view-document file.
			echo $this->renderer->render(
				'myaccount/view-document',
				[
					'get_pdf_url'     => $getPdfUrl,
					'document_number' => $number,
					'type_name_label' => $type_name_label,
				]
			);
            //phpcs:enable
		}
	}
}
