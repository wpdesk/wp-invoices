<?php

namespace WPDesk\Invoices\Documents;

use WPDesk\Invoices\Metabox\OrderMetaboxFields;
use WPDesk\Invoices\Metadata\DocumentMetadata;
use WPDesk\Invoices\Metadata\MetadataContent;
use WPDesk\Invoices\Order\DocumentView;
use WPDesk\PluginBuilder\Plugin\HookableCollection;
use WPDesk\PluginBuilder\Plugin\HookableParent;

/**
 * Class Type
 *
 * Document type.
 *
 * @package WPDesk\Invoices\Documents
 */
abstract class Type implements HookableCollection {

	use HookableParent;

	/**
	 * Document type name.
	 * Type name is used to identify document types in array (key) and is a part of HTML elements ID.
	 *
	 * @var string
	 */
	private $typeName;

	/**
	 * Name for metadata used to save document content.
	 *
	 * @var string
	 */
	private $metaDataName;

	/**
	 * @var string
	 */
	private $metaBoxCreateButtonLabel;

	/**
	 * @var string
	 */
	private $parametersLabel;

	/**
	 * @var OrderMetaboxFields
	 */
	protected $orderMetaboxFields;

	/**
	 * @var AbstractCreator
	 */
	protected $creator;

	/**
	 * @var DocumentView
	 */
	protected $documentView;

	/**
	 * Type constructor.
	 *
	 * @param string             $typeName                 Type name.
	 * @param string             $metaDataName             Meta data name.
	 * @param string             $metaboxCreateButtonLabel Create button label.
	 * @param string             $parametersLabel          Parameters label.
	 * @param OrderMetaboxFields $orderMetaboxFields       Order metabox fields.
	 */
	public function __construct(
		$typeName,
		$metaDataName,
		$metaboxCreateButtonLabel,
		$parametersLabel,
		$orderMetaboxFields
	) {
		$this->typeName                 = $typeName;
		$this->metaDataName             = $metaDataName;
		$this->metaBoxCreateButtonLabel = $metaboxCreateButtonLabel;
		$this->parametersLabel          = $parametersLabel;
		$this->orderMetaboxFields       = $orderMetaboxFields;
	}

	/**
	 * Get typeName.
	 *
	 * @return string
	 */
	public function getTypeName() {
		return $this->typeName;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		$this->hooks_on_hookable_objects();
	}

	/**
	 * @return string
	 */
	public function getMetaDataName() {
		return $this->metaDataName;
	}

	/**
	 * @return string
	 */
	public function getMetaBoxCreateButtonLabel() {
		return $this->metaBoxCreateButtonLabel;
	}

	/**
	 * @param string $metaBoxCreateButtonLabel
	 */
	public function setMetaBoxCreateButtonLabel( $metaBoxCreateButtonLabel ) {
		$this->metaBoxCreateButtonLabel = $metaBoxCreateButtonLabel;
	}

	/**
	 * @return string
	 */
	public function getParametersLabel() {
		return $this->parametersLabel;
	}

	/**
	 * @param string $parametersLabel
	 */
	public function setParametersLabel( $parametersLabel ) {
		$this->parametersLabel = $parametersLabel;
	}

	/**
	 * @param \WC_Order $order
	 *
	 * @return bool
	 */
	public function isAllowedForOrder( $order ) {
		return true;
	}

	/**
	 * @param MetadataContent $metaDataContent
	 *
	 * @return bool
	 */
	public function isMetadataContentValidForDocumentType( MetadataContent $metaDataContent ) {
		return '' !== $metaDataContent->get();
	}

	/**
	 * @return OrderMetaboxFields
	 */
	public function getOrderMetaboxFields() {
		return $this->orderMetaboxFields;
	}

	/**
	 * @return AbstractCreator
	 */
	public function getCreator() {
		return $this->creator;
	}

	/**
	 * @param AbstractCreator $creator
	 */
	public function setCreator( $creator ) {
		$this->creator = $creator;
	}

	/**
	 * @return DocumentView|null
	 */
	public function getDocumentView() {
		return $this->documentView;
	}

	/**
	 * @param DocumentView $documentView
	 */
	public function setDocumentView( $documentView ) {
		$this->documentView = $documentView;
	}

	/**
	 * Get document PDF.
	 *
	 * @param string $documentId
	 *
	 * @return string
	 */
	abstract public function getDocumentPdf( $documentId );

	/**
	 * Prepare Document Metadata.
	 *
	 * @param MetadataContent $metadataContent
	 *
	 * @return DocumentMetadata
	 */
	abstract public function prepareDocumentMetadata( MetadataContent $metadataContent );

	/**
	 * @return string
	 */
	abstract public function getEmailClass();

	/**
	 * Show document in my account page.
	 *
	 * @return bool
	 */
	public function show_document_in_my_account() {
		return true;
	}
}
