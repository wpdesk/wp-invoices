<?php

namespace WPDesk\Invoices\Documents;

use WPDesk\Invoices\Metadata\MetadataContent;

/**
 * Document pdf.
 *
 * @package WPDesk\Invoices\Documents
 */
interface Pdf {

	/**
	 * Get Document PDF.
	 *
	 * @param MetadataContent $metadataContent
	 *
	 * @return string
	 */
	public function getDocumentPdf( MetadataContent $metadataContent );
}
