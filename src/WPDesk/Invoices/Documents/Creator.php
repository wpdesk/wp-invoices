<?php

namespace WPDesk\Invoices\Documents;

/**
 * Documents creator.
 *
 * @package WPDesk\Invoices\Documents
 */
interface Creator {

	/**
	 * Create document for order.
	 *
	 * @param \WC_Order $order             Order.
	 * @param array     $data              Posted data.
	 * @param bool      $overwriteExisting Overwrite already created.
	 *
	 * @throws \Exception
	 */
	public function createDocumentForOrder( $order, array $data, $overwriteExisting = true );
}
