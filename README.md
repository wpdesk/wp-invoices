[![pipeline status](https://gitlab.com/wpdesk/wp-invoices/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-invoices/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-invoices/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-invoices/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-invoices/v/stable)](https://packagist.org/packages/wpdesk/wp-invoices) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-invoices/downloads)](https://packagist.org/packages/wpdesk/wp-invoices) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-invoices/v/unstable)](https://packagist.org/packages/wpdesk/wp-invoices) 
[![License](https://poser.pugx.org/wpdesk/wp-invoices/license)](https://packagist.org/packages/wpdesk/wp-invoices) 

WP Invoices Library
===================

