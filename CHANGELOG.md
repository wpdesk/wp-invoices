## [3.0.3] - 2025-02-26
-  Fixed an issue with the display of document download links on the my-account page
-  PHPCS, PHPCBF, PHPStan

## [3.0.2] - 2025-01-08
-  check if order column $metas is array to prevent notice

## [3.0.1] - 2024-12-12
-  check if order column $metas is array to prevent notice

## [3.0.0] - 2024-12-10
-  support for multiple invoice urls

## [2.4.6] - 2024-11-14
-  auto issue for order statuses on order created

## [2.4.5] - 2024-10-08
-  http library update

## [2.4.4] - 2024-06-26
-  fix client data

## [2.4.3] - 2024-06-04
-  set client data based on woocommerce option

## [2.4.2] - 2024-05-24
-  empty country fix

## [2.4.1] - 2023-12-19
-  escaping html for maintenance mode

## [2.4.0] - 2022-12-19
-  Unslash post data

## [2.3.9] - 2022-11-23
-  WooCommerce 7.1 metabox
-  Columns

## [2.3.8] - 2022-09-01
-  Added process locker

## [2.3.7] - 2022-07-18
-  Added support for negative tax fee item

## [2.3.6] - 2022-04-25
-  Fixed the document generation when a proforma is issued

## [2.3.6] - 2022-03-17
-  Added 1s sleep when WooCommerce status is changed

## [2.3.5] - 2022-01-31
-  Update libraries

## [2.3.4] - 2021-07-15
-  Fixed order date & template

## [2.3.3] - 2020-10-15
-  Property public

## [2.3.2] - 2020-10-14
-  Fixed email hook

## [2.3.1] - 2020-07-12
-  Fixed item variations

## [2.3.0] - 2020-06-25
-  Added download URL for email

## [2.2.3] - 2020-06-24
-  Added setOption for metabox select

## [2.2.2] - 2020-05-25
-  Added log meta

## [2.2.1] - 2020-04-15
-  Fixed FCF compatibility - order edit page and I want invoice field

## [2.2.0] - 2020-04-15
-  Added condition for documents in My Account

## [2.1.3] - 2019-12-09
### Changed
-  Fixed for automatic multiple document issuing

## [2.1.2] - 2019-10-07
### Changed
-  Fixed invoice items in InvoiceData - default empty array

## [2.1.1] - 2019-09-19
### Changed
-  Fixed prefixer

## [2.1.0] - 2019-09-17
### Changed
-  Adjusted to prefixer

## [2.0.4] - 2019-09-02
### Fixed
-  Fixed exception catching when error in status change is thrown

## [2.0.3] - 2019-06-18
### Fixed
- Remove empty rates on tax class 

## [2.0.2] - 2019-06-18
### Fixed
- Restore admin column info

## [2.0.1] - 2019-06-13
### Fixed
- Fixed empty tax rate

## [2.0.0] - 2019-06-03
### Fixed
- Removed Integration from classes

## [1.4.1] - 2019-05-29
### Fixed
- Address fields(like NIP) added multiple times with multiple integrations

## [1.4.0] - 2019-05-28
### Added
- Standard metabox fields
- OrderDocumentView class

## [1.3.0] - 2019-05-28
### Added
- new method in ajax handler getAjaxActionName
### Fixed
- fixed error when downloading pdf with more than one integration

## [1.2.7] - 2019-05-27
### Fixed
- metabox js refactor
- js added and fires twice
- ajax handle fires only for given integration

## [1.2.4] - 2019-05-13
### Fixed
- company name

## [1.2.3] - 2019-04-26
### Fixed
- invoice items creation - item always must be created
