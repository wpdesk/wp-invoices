<?php

use \WPDesk\Invoices\Data\InvoiceData;

/**
 * Class TestInvoiceData
 */
class TestInvoiceData extends WP_UnitTestCase
{

    /**
     * Test CreateFromOrder.
     */
    public function testCreateFromOrder()
    {

        update_option('woocommerce_calc_taxes', 'yes');
        update_option('woocommerce_prices_include_tax', 'no');

        $customer_location = WC_Tax::get_tax_location();
        $tax_rate          = array(
            'tax_rate_country'  => '*',
            'tax_rate_state'    => '',
            'tax_rate'          => '23.0000',
            'tax_rate_name'     => 'VAT',
            'tax_rate_priority' => '1',
            'tax_rate_compound' => '0',
            'tax_rate_shipping' => '1',
            'tax_rate_order'    => '1',
            'tax_rate_class'    => '',
        );

        $tax_rate_id = WC_Tax::_insert_tax_rate($tax_rate);

        $product = WC_Helper_Product::create_simple_product();
        $product->set_regular_price(100);
        $product->set_price(100);
        $product->save();

        $order = WC_Helper_Order::create_order(1, $product);
        $order->calculate_totals();
        $order->save();

        $vatNumber = new \WPDesk\Invoices\Field\VatNumber('nip', 'Nip', 'Nip');

        $invoiceData = InvoiceData::createFromOrder($order, $vatNumber, '');

        $items = $invoiceData->getItems();
        foreach ($items as $item) {
            $this->assertEquals(23, $item->getTaxRate());
            if ($item instanceof \WPDesk\Invoices\Data\Items\LineItem) {
                $this->assertEquals(4, $item->getQuantity());
                $this->assertEquals(100, $item->getPrice());
            }
            if ($item instanceof \WPDesk\Invoices\Data\Items\Shippingtem) {
                $this->assertEquals(1, $item->getQuantity());
                $this->assertEquals(10, $item->getPrice());
            }
        }
    }

}
