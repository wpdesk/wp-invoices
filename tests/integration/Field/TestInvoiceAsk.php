<?php

use \WPDesk\Invoices\Field\InvoiceAsk;

/**
 * Class TestVatNumber
 */
class TestInvoiceAsk extends WP_UnitTestCase
{

    /**
     * Test Hooks.
     */
    public function testHooks()
    {
        $vatNumber = new InvoiceAsk('nip', 'label', 'placeholder');
        $vatNumber->hooks();
        $this->assertEquals(10, has_filter('woocommerce_billing_fields', array($vatNumber, 'appendFieldToWCBillingFields')));
    }

    /**
     * Test addBillingField.
     */
    public function testAddBillingField()
    {
        $vatNumber = new InvoiceAsk('faktura', 'label');
        $vatNumber->hooks();
        $billingFields = WC()->countries->get_address_fields('PL', 'billing_');
        $this->assertEquals(array(
            'label'    => 'label',
            'required' => false,
            'class'    => array( 'form-row-wide' ),
            'type'     => 'checkbox',
            'clear'    => true,
            'priority' => 30,
        ), $billingFields['billing_faktura']);
    }

    /**
     * Test addMetaDataToOrder.
     */
    public function testAddMetaDataToOrder()
    {
        $invoiceAsk = new InvoiceAsk('faktura', 'label');
        $invoiceAsk->hooks();
        $order = WC_Helper_Order::create_order();
        $data = array('billing_faktura' => 1);
        $invoiceAsk->addMetaDataToOrder($order, $data);
        $order->save();
        $this->assertEquals('1', $order->get_meta('_billing_faktura'));
    }

}
