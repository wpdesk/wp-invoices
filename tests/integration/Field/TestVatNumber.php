<?php

use \WPDesk\Invoices\Field\VatNumber;

/**
 * Class TestVatNumber
 */
class TestVatNumber extends WP_UnitTestCase
{

    /**
     * Test Hooks.
     */
    public function testHooks()
    {
        $vatNumber = new VatNumber('nip', 'label', 'placeholder');
        $vatNumber->hooks();
        $this->assertEquals(10, has_filter('woocommerce_billing_fields', array($vatNumber, 'appendFieldToWCBillingFields')));
    }

    /**
     * Test addBillingField.
     */
    public function testAddBillingField()
    {
        $vatNumber = new VatNumber('nip', 'label', 'placeholder');
        $vatNumber->hooks();
        $billingFields = WC()->countries->get_address_fields('PL', 'billing_');
        $this->assertEquals(array(
            'label'       => 'label',
            'placeholder' => 'placeholder',
            'required'    => false,
            'class'       => array( 'form-row-wide' ),
            'clear'       => true,
            'priority'    => 30,
        ), $billingFields['billing_nip']);
    }

    /**
     * Test addMetaDataToOrder.
     */
    public function testAddMetaDataToOrder()
    {
        $vatNumber = new VatNumber('nip', 'label', 'placeholder');
        $vatNumber->hooks();
        $order = WC_Helper_Order::create_order();
        $data = array('billing_nip' => '123456789');
        $vatNumber->addMetaDataToOrder($order, $data);
        $order->save();
        $this->assertEquals('123456789', $order->get_meta('_billing_nip'));
    }


}
