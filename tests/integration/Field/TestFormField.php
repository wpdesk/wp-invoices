<?php

use \WPDesk\Invoices\Field\InvoiceAsk;

/**
 * Class TestVatNumber
 */
class TestFormField extends WP_UnitTestCase
{

    /**
     * Test Hooks.
     */
    public function testHooks()
    {
        $vatNumber = new InvoiceAsk('nip', 'label', 'placeholder');
        $vatNumber->hooks();
        $this->assertEquals(
            10,
            has_filter('woocommerce_billing_fields', array($vatNumber, 'appendFieldToWCBillingFields'))
        );
        $this->assertEquals(
            10,
            has_action('woocommerce_checkout_create_order', array($vatNumber, 'addMetaDataToOrder'))
        );
        $this->assertEquals(
            10,
            has_filter('woocommerce_admin_billing_fields', array($vatNumber, 'addAdminBillingField'))
        );
    }


}
